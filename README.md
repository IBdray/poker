<h1>Poker</h1>
Created on Unreal Engine 5 with usage of C++ and Blueprints.

Simple game Poker in 3D space. Development started in February 2022.
All game logic, AI logic, Map, Interface and some of other assets was created by IB Dray (i.e. Me).
Some other assets were obtained under a Royalty free license from the sites listed below, great thanks to all of them:

<a href="https://www.flaticon.com/">Flaticon: </a>
<ul>
    <li>Playing card icons created by bearicons.</li>
    <li>Close icon created by Pixel perfect.</li>
    <li>Crown, Poker Chips, Fire, Star, Dices icons created by Freepik.</li>
    <li>Leaf icon created by Roundicons.</li>
    <li>Money Bag icon created by mikan933</li>
</ul>

<a href="https://kenney.nl/">Kenney: </a>
<ul>
    <li>Playing Cards and Poker Chips sounds.</li>
</ul>
    
Music from <a href="http://www.twinmusicom.org/">Twin Musicom: </a>
<ul>
    <li>Talky Beat, Rhodesia MkII, Rhodesia Background, Old Bossa, Marxist Arrow.</li>
</ul>
    
<a href="www.zapsplat.com">ZapSplat: </a>
<ul>
    <li>Door opening sound.</li>
    <li>Music from Kevin MacLeon - Slow Burn, Past Sadness, Smooth Lovin.</li>
</ul>
    
All fonts was taken from <a href="https://fonts.google.com/">Google Fonts</a>.

Some of static mesh assets was taken from <a href="https://www.turbosquid.com/">TurboSquid</a>.

HDRI was taken from <a href="https://polyhaven.com/">Poly Haven</a>.
