// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class PokerEditorTarget : TargetRules
{
	public PokerEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		// Open comment for dependencies check
		// === Use Carefully | Very Slow Compile Time ===
		//bUseUnityBuild = false;
		//bUsePSCFiles = false;

		ExtraModuleNames.AddRange( new string[] { "Poker" } );
	}
}
