// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.h"
#include "Components/ActorComponent.h"
#include "ActionConditionManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class POKER_API UActionConditionManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActionConditionManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	EPlayerActionCondition ActionCondition;

public:	
	UFUNCTION(BlueprintCallable)
	EPlayerActionCondition GetCurrentCondition() const;
	UFUNCTION(BlueprintCallable)
	void SetCondition(EPlayerActionCondition Condition);
	
	UFUNCTION(BlueprintCallable)
	bool CheckDefaultCondition();
	UFUNCTION(BlueprintCallable)
	bool CheckMenuCondition();
	UFUNCTION(BlueprintCallable)
	bool CheckRaiseCondition();
		
};
