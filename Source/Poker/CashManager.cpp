// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "CashManager.h"

#include "ChipManager.h"
#include "PokerChip.h"
#include "PokerGameModeBase.h"
#include "Pot.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UCashManager::UCashManager()
{
	PrimaryComponentTick.bCanEverTick = false;
	Cash = 0.0f;
	InitialCash = Cash;

	ChipManager = CreateDefaultSubobject<UChipManager>(TEXT("Chip Manager"));
	ChipManager->SetupAttachment(this);
}

// Called when the game starts
void UCashManager::BeginPlay()
{
	Super::BeginPlay();
}

float UCashManager::PlaceBet(const float Amount)
{
	auto GameMode = Cast<APokerGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	ChipsMovementDelay = GameMode->GetPot()->AddChips(ChipManager->TakeChipsSet(Amount));

	return TakeCash(Amount);
}

float UCashManager::TakeCash(const float Amount)
{
	if (Amount < 0.0f) return 0.0f;
	if (Amount > Cash)
	{
		float Temp = Cash;
		Cash = 0.0f;
		return Temp;
	}
	
	Cash -= Amount;
	return Amount;
}

bool UCashManager::CheckHaveChips()
{
	return ChipManager->GetNumberOfChips() > 0;
}

bool UCashManager::AddAbilityCheck(const float Amount) const
{
	return (Amount > 0.0f && Amount < MaxCash && (Cash + Amount < MaxCash));
}

void UCashManager::AddCash(const float Amount)
{
	Cash += (AddAbilityCheck(Amount)) ? Amount : (Cash + Amount > MaxCash) ? (MaxCash - Cash) : 0.0f;
}

void UCashManager::SetInitialCash(const float Amount)
{
	Cash = Amount;
	InitialCash = Cash;
}

void UCashManager::UpdateInitialCash()
{
	InitialCash = Cash;
}

bool UCashManager::CheckAllIn(const float Bet)
{
	return (Bet >= InitialCash || Cash == 0.0f);
}

void UCashManager::InitChips(const FTransform& ChipsPosition, TSubclassOf<APokerChip> ChipsClass)
{
	ChipManager->PokerChipClass = ChipsClass;
	ChipManager->Init(ChipsPosition, InitialCash);
}

UChipManager* UCashManager::GetChipManager() const
{
	return ChipManager;
}

float UCashManager::GetChipsMovementDelay() const
{
	return ChipsMovementDelay;
}

void UCashManager::SetChipsMovementDelay(float Time)
{
	ChipsMovementDelay = Time;
}

float UCashManager::GetCash() const
{
	return Cash;
}

