// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "CommunityHand.h"

#include "HandManager.h"
#include "PlayingCard.h"
#include "PokerGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACommunityHand::ACommunityHand()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Spacing = 7.0f;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);

	Hand = CreateDefaultSubobject<UHandManager>(TEXT("Community Hand"));
	
}

// Called when the game starts or when spawned
void ACommunityHand::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ACommunityHand::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACommunityHand::SetHandPosition(const FTransform& Position)
{
	Hand->EmptyPositions();
	SetActorTransform(Position);
	
	for (int i = 0; i < MaxNumOfCards; ++i)
	{
		FTransform NewPosition(FRotator::ZeroRotator, FVector(Spacing, 0.0f, 0.0f));
		Scene->AddLocalTransform(NewPosition);
		Hand->AddCardPosition(GetActorTransform());
	}
}

void ACommunityHand::AddCard(APlayingCard* Card)
{
	Hand->AddToHand(Card);
	Card->OnSlideAndPlace();
}

void ACommunityHand::EmptyHand()
{
	Hand->ReturnCardsToDeck();
}

UHandManager* ACommunityHand::GetHand() const
{
	return Hand;
}

void ACommunityHand::Init()
{
	APokerGameModeBase* PokerGameMode = Cast<APokerGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	MaxNumOfCards = PokerGameMode->GetInitialInfo().NumOfCommunityCards;
	Hand->SetNumberOfCards(MaxNumOfCards);
}

