// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.h"
#include "PokerChipsValue.h"
#include "InitialGameInfo.h"
#include "GameFramework/GameModeBase.h"
#include "PokerGameModeBase.generated.h"


class APlayerPawn;
class APlayersManager;

class ACommunityHand;
class ADeckOfCards;

class APot;

class APokerChip;
class AWhiteChip;


/**
 * 
 */
UCLASS()
class POKER_API APokerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	APokerGameModeBase();
	virtual void BeginPlay() override;

protected:
	UFUNCTION(BlueprintCallable)
	void StartPokerGame();
	UFUNCTION(BlueprintCallable)
	void StartMatch();

	UFUNCTION(BlueprintCallable)
	void FirstRound();
	UFUNCTION(BlueprintCallable)
	void SecondRound();
	UFUNCTION(BlueprintCallable)
	void ThirdRound();
	UFUNCTION(BlueprintCallable)
	void AllInRound();
	UFUNCTION(BlueprintCallable)
	void FinishMatch();

	UFUNCTION(BlueprintCallable)
	void EndPokerGame();

	UFUNCTION(BlueprintCallable)
	void SplitPot(TArray<APlayerPawn*> Players, float CashPrize, TArray<APokerChip*> ChipsPrize);
	UFUNCTION(BlueprintCallable)
	TArray<APlayerPawn*> CalcWinners(const TArray<APlayerPawn*>& Players);
	UFUNCTION(BlueprintCallable)
	APlayerPawn* GetLowestBetPlayer(const TArray<APlayerPawn*>& Players);
	UFUNCTION(BlueprintCallable)
	float CalcMaxPrizeAmount(const APlayerPawn* Player);
	UFUNCTION(BlueprintCallable)
	int32 CalcChipsPrizeFromCashPrize(float AllPrize, float PrizePart, int32 ChipsNum);

	UFUNCTION(BlueprintCallable)
	void InitMatch();
	UFUNCTION(BlueprintCallable)
	void InitPlayersHands();

	/*
Add certain number of cards to community hand if fit and update rank of players hands
	 */
	UFUNCTION(BlueprintCallable)
	void PlaceCommunityCards(const int32 NumOfCards);

	TArray<APokerChip*> SeparateChips(TArray<APokerChip*>& ChipsSet, int32 AmountToSeparate);


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	bool bGameStarted;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	bool bAllIn;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	bool bSmallBlindGiven;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	bool bBigBlindGiven;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	int32 RaiseCapCounter;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	bool bOpeningRaise;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	ERoundCounter RoundCounter;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	float LastBet;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	float MinRaise;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Info")
	TArray<APlayerPawn*> WinnersArray;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game References")
	ADeckOfCards* DeckOfCards;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game References")
	APlayersManager* PlayersManager;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game References")
	ACommunityHand* CommunityHand;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game References")
	AWhiteChip* WhiteChip;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game References")
	APot* Pot;

	// === Game Properties ===
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Properties")
	bool bEnableRaiseCap;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Properties")
	bool bEnableBlinds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Properties")
	int32 NumOfRaisesBeforeCap;

	// === Chip Values ===
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chip Values")
	float RedChipValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chip Values")
	float BlueChipValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chip Values")
	float GreenChipValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chip Values")
	float BlackChipValue;

	// === Initial Values ===
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Info")
	float WalletInitialCash;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Info")
	float PotInitialCash;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Info")
	float MinRaiseAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Info")
	int32 NumOfCardsHeldByPlayers;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Info")
	int32 NumOfCommunityCards;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Info")
	int32 NumOfChipTypes;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Initial Info")
	float SmallBlindValue;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Initial Info")
	float BigBlindValue;
	

	// === Delay Properties ===
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float StartMathDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float InitHandsDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float SmallBlindDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float BigBlindDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float CommunityCardsDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float AllInRoundDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float NextMatchDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float FinishMatchDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float InitPlayerHandsDelay;
	UPROPERTY(EditAnywhere, Category = "Delay Properties")
	float PlaceCommunityCardsDelay;

	// === Class Defaults ===
	UPROPERTY(EditAnywhere, Category = "Class Defaults")
	TSubclassOf<ADeckOfCards> DeckClass;
	UPROPERTY(EditAnywhere, Category = "Class Defaults")
	TSubclassOf<APlayersManager> PlayersManagerClass;
	UPROPERTY(EditAnywhere, Category = "Class Defaults")
	TSubclassOf<AWhiteChip> WhiteChipClass;
	UPROPERTY(EditAnywhere, Category = "Class Defaults")
	TSubclassOf<ACommunityHand> CommunityHandClass;
	UPROPERTY(EditAnywhere, Category = "Class Defaults")
	TSubclassOf<APot> PotClass;

public:
	UFUNCTION(BlueprintCallable)
	void Turn();
	UFUNCTION(BlueprintCallable)
	void NextRound();
	UFUNCTION(BlueprintCallable)
	void NextMatch();

	UFUNCTION(BlueprintCallable)
	bool CheckCanRaise() const;
	UFUNCTION(BlueprintCallable)
	void IncrementRaiseCounter();
	
	UFUNCTION(BlueprintCallable)
	void AddToPot(const float Amount);
	UFUNCTION(BlueprintCallable)
	float TakeFromPot();

	UFUNCTION(BlueprintCallable)
	void SetLastBet(const float Amount);
	UFUNCTION(BlueprintCallable)
	void SetMinRaise(const float Amount);

	UFUNCTION(BlueprintCallable)
	APot* GetPot() const;
	UFUNCTION(BlueprintCallable)
	ADeckOfCards* GetDeckOfCards() const;
	UFUNCTION(BlueprintCallable)
	APlayersManager* GetPlayersManager() const;
	UFUNCTION(BlueprintCallable)
	AWhiteChip* GetWhiteChip() const;
	UFUNCTION(BlueprintCallable)
	ACommunityHand* GetCommunityHand() const;
	
	UFUNCTION(BlueprintCallable)
	bool GetGameStarted() const;
	UFUNCTION(BlueprintCallable)
	bool GetAllIn() const;
	UFUNCTION(BlueprintCallable)
	bool GetSmallBlindGiven() const;
	UFUNCTION(BlueprintCallable)
	bool GetBigBlindGiven() const;
	UFUNCTION(BlueprintCallable)
	const TArray<APlayerPawn*>& GetWinners() const;
	UFUNCTION(BlueprintCallable)
	float GetLastBet() const;
	UFUNCTION(BlueprintCallable)
	float GetMinRaise() const;
	
	UFUNCTION(BlueprintCallable)
	FPokerChipsValue GetChipsValue() const;
	UFUNCTION(BlueprintCallable)
	FInitialGameInfo GetInitialInfo() const;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTransform PotPosition;
	
	// === Native Events ===
	UFUNCTION(BlueprintImplementableEvent)
    void OnMatchInit();
	UFUNCTION(BlueprintImplementableEvent)
	void OnMatchStart();
	UFUNCTION(BlueprintImplementableEvent)
	void OnFirstRound();
	UFUNCTION(BlueprintImplementableEvent)
	void OnSecondRound();
	UFUNCTION(BlueprintImplementableEvent)
	void OnThirdRound();
	UFUNCTION(BlueprintImplementableEvent)
    void OnMatchFinish();
	UFUNCTION(BlueprintImplementableEvent)
	void OnGameEnd();
    	
	UFUNCTION(BlueprintImplementableEvent)
	void OnAllInRound();
	UFUNCTION(BlueprintImplementableEvent)
	void OnCreateSidePot();
	
	UFUNCTION(BlueprintImplementableEvent)
	void OnCardToHandAddition();
	UFUNCTION(BlueprintImplementableEvent)
	void OnHandOpening();
	UFUNCTION(BlueprintImplementableEvent)
	void OnPotChange();

	UFUNCTION(BlueprintImplementableEvent)
	void OnWinChipsStartMovement();
	UFUNCTION(BlueprintImplementableEvent)
	void OnWinChipsEndMovement();
};
