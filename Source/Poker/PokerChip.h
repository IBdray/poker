// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Actor.h"
#include "PokerChip.generated.h"


UCLASS()
class POKER_API APokerChip : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APokerChip();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void TimelineMovementProcess(float DeltaTime);
	UFUNCTION()
	void TimelineMovementFinish();
	UFUNCTION()
	void MovementEndSound();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Scene;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAudioComponent* Audio;
	
	UPROPERTY(meta=(AllowedClasses="StaticMesh"))
	FSoftObjectPath SoftObjectPath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCurveFloat* MovementCurve;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Value;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EChipColor Color;

private:
	UPROPERTY(EditAnywhere)
	float TimeOfMovementEndSound;

	UPROPERTY(EditAnywhere)
	bool bPlayMovementEndSound;
	UPROPERTY(EditAnywhere)
	bool bDestroyAfterMove;
	UPROPERTY(EditAnywhere)
	float ChipHeight;
	
	FTransform MovementPosition;
	FTimeline MovementTimeline;
	
public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Move(const FTransform& NewPosition, bool PlayMovementEndSound = true);

	UFUNCTION(BlueprintImplementableEvent)
	void OnEndMovement();

	UFUNCTION(BlueprintCallable)
	float GetValue() const;
	UFUNCTION(BlueprintCallable)
	EChipColor GetColor() const;
	UFUNCTION(BlueprintCallable)
	UCurveFloat* GetMovementCurve() const;
	UFUNCTION(BlueprintCallable)
	float GetChipHeight() const;

	void Init(const EChipColor ChipColor);
};
