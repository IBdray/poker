// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "HandManager.h"

#include "PlayingCard.h"

UHandManager::UHandManager()
{
	PrimaryComponentTick.bCanEverTick = false;
	NumberOfCardsInHand = 2;
	
	bHandOpen = false;
	bHaveEnoughCards = false;

	HandRank = "00000";
	EnumHandRank = EHandRank::HR_HighCard;
	HighCard = ECardPower::CP_2;
}

void UHandManager::AddCardPosition(const FTransform Position)
{
	if (CardsPositions.Num() < NumberOfCardsInHand) CardsPositions.Emplace(Position);
}

void UHandManager::EmptyPositions()
{
	CardsPositions.Empty();
}

void UHandManager::AddToHand(APlayingCard* Card)
{
	if (Card && Cards.Num() < NumberOfCardsInHand)
	{
		Cards.Emplace(Card);
		Card->Place(GetCardTransformByIndex(Cards.Num() - 1), true);
	}
	if (Cards.Num() == NumberOfCardsInHand)
	{
		float StartTime, EndTime;
		Card->GetMovementCurve()->GetTimeRange(StartTime, EndTime);
		
		FTimerHandle Timer;
		GetWorld()->GetTimerManager().SetTimer(Timer, [=]()
		{
			bHaveEnoughCards = true;
		}, 0.1f, false, EndTime + 0.5f);
	}
}

void UHandManager::OpenHand()
{
	bHandOpen = true;
	
	for (int32 i = 0; i < Cards.Num(); ++i)
	{
		FTransform Position = GetCardTransformByIndex(i);
		FRotator NewRotation(Position.Rotator().Pitch + 180.0f, Position.Rotator().Yaw, Position.Rotator().Roll);

		Cards[i]->Place(FTransform(NewRotation, Position.GetLocation()), false);
		Cards[i]->OnOpen();
	}
}

void UHandManager::HideHand()
{
	bHandOpen = false;
}

void UHandManager::ReturnCardsToDeck()
{
	const float Speed = 0.15f;
	float FinishTime = Speed; 
	for (int32 i = 0; i < Cards.Num(); ++i)
	{
		APlayingCard* Card = Cards[i];
		FinishTime += Speed * i;
		
		FTimerHandle Timer;
		GetWorld()->GetTimerManager().SetTimer(Timer, [=]()
		{
			Card->Move(Cards.Last()->GetActorTransform());
			
			if (i != Cards.Num() - 1)
				Card->OnShovel();
		}, 0.1f, false, Speed * i);
	}

	FTimerHandle Timer;
	GetWorld()->GetTimerManager().SetTimer(Timer, [=]()
	{
		for (int32 i = 0; i < Cards.Num(); ++i)
		{
			Cards[i]->ReturnToDeck();
			Cards[i] = nullptr;
		}
		Cards.Empty();
	}, 0.1f, false, FinishTime);

	bHaveEnoughCards = false;
	ResetHandRank();
}

// === Default Methods ===
void UHandManager::BeginPlay()
{
	Super::BeginPlay();
}


bool UHandManager::CheckEnoughCards() const
{
	return bHaveEnoughCards;
}

bool UHandManager::CheckIsStraight(const TArray<int32>& ArrayOfCards)
{
	int32 SequenceCounter = 0;
	for (const auto Item : ArrayOfCards)
	{
		if (Item == 1) ++SequenceCounter;
		if (SequenceCounter > 0 && Item != 1) break;
	}
	return SequenceCounter == 5;
}

bool UHandManager::CheckIsFlush(const TArray<APlayingCard*>& Hand)
{
	const ECardSuit Suit = Hand[0]->GetSuit();
	for (const auto Card : Hand)
		if (Card->GetSuit() != Suit)
			return false;
	return true;
}

FString UHandManager::RankHand(const TArray<APlayingCard*>& Hand)
{
	if (Hand.Num() != 5) return "00000";
	
	const bool bFlush = CheckIsFlush(Hand);
	TArray<int32> CardsCount;
	CardsCount.Init(0, 15);
	for (const auto Card : Hand) ++CardsCount[static_cast<int32>(Card->GetPower()) + 2];
	if (CardsCount[2] == 1 && CardsCount[3] == 1 && CardsCount[4] == 1 && CardsCount[5] && CardsCount[14] == 1)
	{
		CardsCount[1] = 1;
		CardsCount[14] = 0;
	}
	
	const bool bStraight = CheckIsStraight(CardsCount);
	
	FString Rank, TieBreaker;
	for (int32 Amount = 4; Amount > 0; --Amount)
		for (int32 Power = 14; Power > 1; --Power)
			if (CardsCount[Power] == Amount)
				{
					Rank.AppendChar('0' + Amount);
					TieBreaker.AppendChar(Power < 10 ? '0' + Power : 'A' + (Power - 10));
				}
	
	if (Rank.Len() == 2) Rank.AppendChar('0');
	if (Rank.Len() == 3) Rank.AppendChar('0');
	if (Rank.Len() == 4) Rank.AppendChar('0');

	if (CardsCount[14] == 1 && bStraight && bFlush)
		Rank = "6ROFL";
	else if (bStraight)
		Rank = bFlush ? "5STFL" : "312ST";
	else if (bFlush)
		Rank = "313FL";
	
	return Rank + "-" + TieBreaker;
}

FString UHandManager::RankTwoCardsHand(const TArray<APlayingCard*>& Hand)
{
	if (Hand.Num() != 2)
		return "00000";
	if  (Hand[0]->GetPower() == Hand[1]->GetPower())
		return "20000-" + Hand[0]->GetCharPower();

	const auto bIsFirstCardStronger = Hand[0]->GetPower() > Hand[1]->GetPower();
	const FString TieBreaker = bIsFirstCardStronger ? Hand[0]->GetCharPower() + Hand[1]->GetCharPower()
													: Hand[1]->GetCharPower() + Hand[0]->GetCharPower();
	
	return "11000-" + TieBreaker;
}

FString UHandManager::ShrinkAndRankHand(TArray<APlayingCard*> Hand, int32 RemoveIndex)
{
	if (Hand.Num() == 5)
		return RankHand(Hand);
	if (Hand.Num() == 2)
		return RankTwoCardsHand(Hand);
	
	FString BestHand = "";
	if (RemoveIndex > 0 && RemoveIndex <= Hand.Num())
		Hand.RemoveAt(Hand.Num() - RemoveIndex);

	for (int32 i = 0; i < Hand.Num(); ++i)
	{
		FString NewHand = ShrinkAndRankHand(Hand, i + 1);
		if (NewHand > BestHand) BestHand = NewHand;
	}

	return BestHand;
}

void UHandManager::HandRanking(TArray<APlayingCard*> ArrayOfCards)
{
	if (Cards.Num() == NumberOfCardsInHand)
	{
		TArray<APlayingCard*> CardsArray;
		CardsArray.Reserve(ArrayOfCards.Num() + NumberOfCardsInHand);
		for (const auto Card : Cards)
			CardsArray.Emplace(Card);
		for (const auto Card : ArrayOfCards)
			CardsArray.Emplace(Card);
		CardsArray.Sort();

		HandRank = ShrinkAndRankHand(CardsArray);
		const FString TempRank = HandRank.Left(5);
		if (TempRank == "11111")
			EnumHandRank = EHandRank::HR_HighCard;
		else if (TempRank == "21110")
			EnumHandRank = EHandRank::HR_Pair;
		else if (TempRank == "22100")
			EnumHandRank = EHandRank::HR_TwoPair;
		else if (TempRank == "31100")
			EnumHandRank = EHandRank::HR_ThreeOfKind;
		else if (TempRank == "312ST")
			EnumHandRank = EHandRank::HR_Straight;
		else if (TempRank == "313FL")
			EnumHandRank = EHandRank::HR_Flush;
		else if (TempRank == "32000")
			EnumHandRank = EHandRank::HR_FullHouse;
		else if (TempRank == "5STFL")
			EnumHandRank = EHandRank::HR_StraightFlush;
		else if (TempRank == "6ROFL")
			EnumHandRank = EHandRank::HR_RoyalFlush;


		int32 HighCardChar = static_cast<int32>(HandRank[6]);
		int32 HighCardPower = HighCardChar >= 65 ? HighCardChar - 57 : HighCardChar - 50;
		HighCard = static_cast<ECardPower>(HighCardPower);
	}
}

EMatchResult UHandManager::CompareHands(const UHandManager* Other)
{
	return (HandRank > Other->HandRank) ? EMatchResult::MR_Win :
		   (HandRank < Other->HandRank) ? EMatchResult::MR_Lose : EMatchResult::MR_Tie;
}

void UHandManager::ResetHandRank()
{
	HandRank = "00000";
	EnumHandRank = EHandRank::HR_HighCard;
	HighCard = ECardPower::CP_2;
}

// === Getters ===
APlayingCard* UHandManager::GetLastCard() const
{
	return (Cards.Num() > 0) ? Cards.Last() : nullptr;
}
const TArray<APlayingCard*>& UHandManager::GetCardsArray() const
{
	return Cards;
}
FTransform UHandManager::GetCardTransformByIndex(const int32 Index) const
{
	if (Index < CardsPositions.Num() && Index >= 0)
		return CardsPositions[Index];
	return FTransform(FRotator::ZeroRotator, FVector::ZeroVector);
}
FString UHandManager::GetHandRank() const
{
	return HandRank;
}
EHandRank UHandManager::GetEnumRank() const
{
	return EnumHandRank;
}

ECardPower UHandManager::GetHighCard() const
{
	return HighCard;
}

bool UHandManager::CheckIfHandOpen() const
{
	return bHandOpen;
}


// === Setters ===
// Should Be Used In Constructor To Reserve Space In Array
void UHandManager::SetNumberOfCards(const int32 Number)
{
	NumberOfCardsInHand = Number;
	Cards.Reserve(NumberOfCardsInHand);
	CardsPositions.Reserve(NumberOfCardsInHand);
}
