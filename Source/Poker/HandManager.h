// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.h"
#include "PlayingCardsEnums.h"
#include "Components/ActorComponent.h"
#include "HandManager.generated.h"

class APlayingCard;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class POKER_API UHandManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHandManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<APlayingCard*> Cards;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FTransform> CardsPositions;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString HandRank;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EHandRank EnumHandRank;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ECardPower HighCard;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHandOpen;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHaveEnoughCards;
	
private:
	int32 NumberOfCardsInHand;

	bool CheckIsStraight(const TArray<int32>& ArrayOfCards);
	bool CheckIsFlush(const TArray<APlayingCard*>& Hand);
	
	FString RankHand(const TArray<APlayingCard*>& Hand);
	FString RankTwoCardsHand(const TArray<APlayingCard*>& Hand);
	FString ShrinkAndRankHand(TArray<APlayingCard*> Hand, int32 RemoveIndex = 0);
	
public:
	UFUNCTION(BlueprintCallable)
	void SetNumberOfCards(const int32 Number);

	UFUNCTION(BlueprintCallable)
	void HandRanking(TArray<APlayingCard*> ArrayOfCards);
	UFUNCTION(BlueprintCallable)
	EMatchResult CompareHands(const UHandManager* Other);
	UFUNCTION(BlueprintCallable)
	void ResetHandRank();
	
	UFUNCTION(BlueprintCallable)
	void AddCardPosition(const FTransform Position);
	UFUNCTION(BlueprintCallable)
	void EmptyPositions();
	

	UFUNCTION(BlueprintCallable)
	void AddToHand(APlayingCard* Card);
	UFUNCTION(BlueprintCallable)
	void ReturnCardsToDeck();
	
	UFUNCTION(BlueprintCallable)
	void OpenHand();
	UFUNCTION(BlueprintCallable)
	void HideHand();

	UFUNCTION(BlueprintCallable)
	APlayingCard* GetLastCard() const;
	UFUNCTION(BlueprintCallable)
	const TArray<APlayingCard*>& GetCardsArray() const;
	UFUNCTION(BlueprintCallable)
	FTransform GetCardTransformByIndex(const int32 Index) const;
	UFUNCTION(BlueprintCallable)
	FString GetHandRank() const;
	UFUNCTION(BlueprintCallable)
	EHandRank GetEnumRank() const;
	UFUNCTION(BlueprintCallable)
	ECardPower GetHighCard() const;
	
	UFUNCTION(BlueprintCallable)
	bool CheckIfHandOpen() const;
	UFUNCTION(BlueprintCallable)
	bool CheckEnoughCards() const;
};
