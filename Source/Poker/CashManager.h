// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "CashManager.generated.h"

class APokerChip;
class UChipManager;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class POKER_API UCashManager : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCashManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	bool AddAbilityCheck(const float Amount) const;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UChipManager* ChipManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float InitialCash;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Cash;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ChipsMovementDelay;
	
private:	
	static constexpr float MaxCash = 999999.0f;
	
public:	
	UFUNCTION(BlueprintCallable)
	float PlaceBet(const float Amount);
	
	UFUNCTION(BlueprintCallable)
	float TakeCash(const float Amount);
	UFUNCTION(BlueprintCallable)
	void AddCash(const float Amount);

	/*
Return current amount of cash
	*/
	UFUNCTION(BlueprintCallable)
	float GetCash() const;
	UFUNCTION(BlueprintCallable)
	void SetInitialCash(const float Amount);
	UFUNCTION(BlueprintCallable)
	void UpdateInitialCash();

	UFUNCTION(BlueprintCallable)
	bool CheckAllIn(const float Bet);
	
	UFUNCTION(BlueprintCallable)
	void InitChips(const FTransform& ChipsPosition, TSubclassOf<APokerChip> ChipsClass);
	UFUNCTION(BlueprintCallable)
	bool CheckHaveChips();

	UFUNCTION(BlueprintCallable)
	UChipManager* GetChipManager() const;
	
	UFUNCTION(BlueprintCallable)
	float GetChipsMovementDelay() const;
	UFUNCTION(BlueprintCallable)
	void SetChipsMovementDelay(float Time);
};
