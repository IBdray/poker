// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PlayingCardsEnums.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class ECardSuit : uint8
{
 CS_Hearts           UMETA(DisplayName = "Hearts"),
 CS_Diamonds         UMETA(DisplayName = "Diamonds"),
 CS_Clubs            UMETA(DisplayName = "Club"),
 CS_Spades           UMETA(DisplayName = "Spades"),
};

UENUM(BlueprintType)
enum class ECardPower : uint8
{
 CP_2                UMETA(DisplayName = "2"),
 CP_3                UMETA(DisplayName = "3"),
 CP_4                UMETA(DisplayName = "4"),
 CP_5                UMETA(DisplayName = "5"),
 CP_6                UMETA(DisplayName = "6"),
 CP_7                UMETA(DisplayName = "7"),
 CP_8                UMETA(DisplayName = "8"),
 CP_9                UMETA(DisplayName = "9"),
 CP_10               UMETA(DisplayName = "10"),
 CP_Jack             UMETA(DisplayName = "Jack"),
 CP_Queen            UMETA(DisplayName = "Queen"),
 CP_King             UMETA(DisplayName = "King"),
 CP_Ace              UMETA(DisplayName = "Ace"),
 //CP_Joker            UMETA(DisplayName = "Joker"),
};

UENUM(BlueprintType)
enum class EHandRank : uint8
{
 HR_HighCard         UMETA(DisplayName = "High Card"),
 HR_Pair             UMETA(DisplayName = "Pair"),
 HR_TwoPair          UMETA(DisplayName = "Two Pair"),
 HR_ThreeOfKind      UMETA(DisplayName = "Three Of Kind"),
 HR_Straight         UMETA(DisplayName = "Straight"),
 HR_Flush            UMETA(DisplayName = "Flush"),
 HR_FullHouse        UMETA(DisplayName = "Full House"),
 HR_FourOfKind       UMETA(DisplayName = "Four Of Kind"),
 HR_StraightFlush    UMETA(DisplayName = "Straight Flush"),
 HR_RoyalFlush       UMETA(DisplayName = "Royal Flush"),
};
