// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerChipsValue.h"
#include "PokerEnums.h"
#include "Components/SceneComponent.h"
#include "ChipManager.generated.h"

class APokerChip;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class POKER_API UChipManager : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UChipManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	TArray<APokerChip*> Chips;
	UPROPERTY(VisibleAnywhere)
	TArray<FVector> ChipsLocations;

	UPROPERTY(EditAnywhere)
	float SpacingY;
	UPROPERTY(EditAnywhere)
	float SpacingX;
	UPROPERTY(EditAnywhere)
	float ChipsInSetMin;
	UPROPERTY(EditAnywhere)
	float ChipsInSetMax;
	UPROPERTY(EditAnywhere)
	int32 NumOfLocations;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Delay Properties")
	float ChipsMovementDelay;

private:
	UPROPERTY(VisibleAnywhere)
	float NumOfChips;
	
	FPokerChipsValue ChipsValue;
	int32 NumOfChipTypes;

public:
	UPROPERTY(VisibleAnywhere)
	TSubclassOf<APokerChip> PokerChipClass;

	UFUNCTION(BlueprintCallable)
	void CreateChipsSet(FVector& Location, const int32 SizeOfSet, float ValueOfSet);
	UFUNCTION(BlueprintCallable)
	void CreateChip(const EChipColor ChipType, FVector& TopLocationOfSet);

	UFUNCTION(BlueprintCallable)
	void AddChips(const TArray<APokerChip*>& ChipsSet);
	UFUNCTION(BlueprintCallable)
	TArray<APokerChip*> TakeChipsSet(float Value);
	UFUNCTION(BlueprintCallable)
	TArray<APokerChip*> TakeAllChips();
	
	UFUNCTION(BlueprintCallable)
	void AddChipsLocation(FVector Location);
	UFUNCTION(BlueprintCallable)
	FVector& GetRandomLocation();

	/*
Return Time for chips to move
	 */
	UFUNCTION(BlueprintCallable)
	float MoveChipsSet(FVector& Location, const TArray<APokerChip*>& ChipsSet);
	/*
Return Time for chips to move
	 */
	UFUNCTION(BlueprintCallable)
	float MoveChipsSetRandom(const TArray<APokerChip*>& ChipsSet, bool PlaySound = true);

	void Init(const FTransform& Position, const float CashAmount);

	UFUNCTION(BlueprintCallable)
	void ReInit(const float CashAmount, bool CreateChipsSets = true);
	
	UFUNCTION(BlueprintCallable)
	int32 GetNumberOfChips() const;
};
