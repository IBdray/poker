// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "WhiteChip.h"

#include "PlayerPawn.h"
#include "Components/AudioComponent.h"

// Sets default values
AWhiteChip::AWhiteChip()
{
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("White Chip"));
	Mesh->SetupAttachment(Scene);

	Audio = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));
	Audio->SetupAttachment(Scene);
	Audio->bAutoActivate = false;
}

// Called when the game starts or when spawned
void AWhiteChip::BeginPlay()
{
	Super::BeginPlay();
	
	FOnTimelineFloat ProgressFunction;
	ProgressFunction.BindUFunction(this, TEXT("TimelineMovementProcess"));
	MovementTimeline.AddInterpFloat(MovementCurve, ProgressFunction);

	FOnTimelineEvent OnTimelineFinishedFunction;
	OnTimelineFinishedFunction.BindUFunction(this, TEXT("TimelineMovementFinish"));
	MovementTimeline.SetTimelineFinishedFunc(OnTimelineFinishedFunction);

	FOnTimelineEvent OnTimelineEnd;
	OnTimelineEnd.BindUFunction(this, TEXT("MovementEndSound"));
	MovementTimeline.AddEvent(TimeOfMovementEndSound, OnTimelineEnd);

	MovementTimeline.SetTimelineLengthMode(TL_LastKeyFrame);
}

void AWhiteChip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MovementTimeline.IsPlaying())
		MovementTimeline.TickTimeline(DeltaTime);
}

void AWhiteChip::TimelineMovementProcess(float Value)
{
	FVector Location = FMath::Lerp(GetActorLocation(), MovementPosition.GetLocation(), Value);
	FRotator Rotation = FMath::Lerp<FRotator>(GetActorRotation(), MovementPosition.Rotator(), Value);
	SetActorLocationAndRotation(Location, Rotation);
}

void AWhiteChip::TimelineMovementFinish()
{
}

void AWhiteChip::MovementEndSound()
{
	OnEndMovement();
}

void AWhiteChip::GiveTo(APlayerPawn* Player)
{
	FreeIfOwned();
	OwnedBy = Player;
	Player->SetWhiteChipOwner(true);
	
	MovementPosition = FTransform(FRotator::ZeroRotator, Player->GetWhiteChipPosition());
	MovementTimeline.PlayFromStart();
}

bool AWhiteChip::CheckIfOwned()
{
	if (OwnedBy != nullptr && OwnedBy.IsValid())
		return OwnedBy.Get()->GetWhiteChipOwner();
	return false;
}
void AWhiteChip::FreeIfOwned()
{
	if (CheckIfOwned()) OwnedBy.Get()->SetWhiteChipOwner(false);
}

