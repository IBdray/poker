// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Actor.h"
#include "DeckOfCards.generated.h"

class APlayingCard;

UCLASS()
class POKER_API ADeckOfCards : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADeckOfCards();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void TimelineMovementProcess(float Value);
	UFUNCTION()
	void TimelineMovementFinish();
	UFUNCTION()
	void MovementEndSound();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Scene;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAudioComponent* Audio;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCurveFloat* MovementCurve;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<APlayingCard*> CardsArray;
	UPROPERTY(EditAnywhere, Category = "Class Defaults")
	TSubclassOf<APlayingCard> CardsClass;

private:
	FTransform MovementPosition;
	FTimeline MovementTimeline;

	UPROPERTY(EditAnywhere)
	float TimeOfMovementEndSound;

	void InitCardsArray();
	void UpdateCardsPosition();

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Place(const FTransform& Position);
	UFUNCTION(BlueprintCallable)
	void ReturnCard(APlayingCard* Card);

	UFUNCTION(BlueprintImplementableEvent)
	void OnStartMovement();
	UFUNCTION(BlueprintImplementableEvent)
	void OnEndMovement();
	
	UFUNCTION(BlueprintCallable)
	APlayingCard* GetLastCard() const;
	UFUNCTION(BlueprintCallable)
	APlayingCard* GetRandomCard() const;
	UFUNCTION(BlueprintCallable)
	APlayingCard* TakeRandomCard();

};
