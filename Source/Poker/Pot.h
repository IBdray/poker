// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pot.generated.h"

class UCashManager;
class APokerChip;

UCLASS()
class POKER_API APot : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Scene;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCashManager* Cash;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FVector> ChipsSetLocations;

public:
	UFUNCTION(BlueprintCallable)
	void PrepareForNewMatch();
	UFUNCTION(BlueprintCallable)
	void SetPosition(const FTransform& Position);
	
	UFUNCTION(BlueprintCallable)
	void AddCash(const float Amount);
	UFUNCTION(BlueprintCallable)
	float TakeAllCash();

	/*
Return Time for chips to move
	 */
	UFUNCTION(BlueprintCallable)
	float AddChips(const TArray<APokerChip*>& ChipsSet);
	UFUNCTION(BlueprintCallable)
	TArray<APokerChip*> TakeAllChips();
	
	UFUNCTION(BlueprintCallable)
	float GetCash() const;

	UFUNCTION(BlueprintCallable)
	void Init();
	UFUNCTION(BlueprintCallable)
	void ResetDefault();
};
