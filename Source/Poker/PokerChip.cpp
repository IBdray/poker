// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "PokerChip.h"

#include "PokerGameModeBase.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APokerChip::APokerChip()
{
	PrimaryActorTick.bCanEverTick = true;
	TimeOfMovementEndSound = 0.2f;
	ChipHeight = 0.45f;
	bPlayMovementEndSound = true;
	
	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Scene);

	Audio = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));
	Audio->SetupAttachment(Scene);
	Audio->bAutoActivate = false;
}

// Called when the game starts or when spawned
void APokerChip::BeginPlay()
{
	Super::BeginPlay();

	FOnTimelineFloat ProgressFunction;
	ProgressFunction.BindUFunction(this, TEXT("TimelineMovementProcess"));
	MovementTimeline.AddInterpFloat(MovementCurve, ProgressFunction);

	FOnTimelineEvent OnTimelineFinishedFunction;
	OnTimelineFinishedFunction.BindUFunction(this, TEXT("TimelineMovementFinish"));
	MovementTimeline.SetTimelineFinishedFunc(OnTimelineFinishedFunction);

	FOnTimelineEvent OnTimelineEnd;
	OnTimelineEnd.BindUFunction(this, TEXT("MovementEndSound"));
	MovementTimeline.AddEvent(TimeOfMovementEndSound, OnTimelineEnd);

	MovementTimeline.SetTimelineLengthMode(TL_LastKeyFrame);
}

void APokerChip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MovementTimeline.IsPlaying())
		MovementTimeline.TickTimeline(DeltaTime);
}

void APokerChip::Move(const FTransform& NewPosition, bool PlaySound)
{
	if (!PlaySound)
		bPlayMovementEndSound = false;
	
	MovementPosition = NewPosition;
	MovementTimeline.PlayFromStart();
}

// === Timeline Methods ===
void APokerChip::TimelineMovementProcess(float DeltaTime)
{
	FVector Location = FMath::Lerp(GetActorLocation(), MovementPosition.GetLocation(), DeltaTime);
	FRotator Rotation = FMath::Lerp<FRotator>(GetActorRotation(), MovementPosition.Rotator(), DeltaTime);
	SetActorLocationAndRotation(Location, Rotation);
}

void APokerChip::TimelineMovementFinish()
{
}

void APokerChip::MovementEndSound()
{
	if (bPlayMovementEndSound)
	{
		bPlayMovementEndSound = true;
		OnEndMovement();
	}
}

void APokerChip::Init(const EChipColor ChipColor)
{
	FPokerChipsValue ChipsValue = Cast<APokerGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()))->GetChipsValue();
	
	switch (ChipColor)
	{
		case EChipColor::CC_Red:
			Value = ChipsValue.RedChipValue;
			break;
		case EChipColor::CC_Blue:
			Value = ChipsValue.BlueChipValue;
			break;
		case EChipColor::CC_Green:
			Value = ChipsValue.GreenChipValue;
			break;
		case EChipColor::CC_Black:
			Value = ChipsValue.BlackChipValue;
			break;
	}
	Color = ChipColor;

	FString ColorText = UEnum::GetValueAsString(ChipColor);
	ColorText.RemoveAt(0, 15);
	FString Path = "StaticMesh'/Game/Poker/PokerAssets/PokerChips/Meshes/SM_PokerChip_" + ColorText + ".SM_PokerChip_" + ColorText + "'";
	SoftObjectPath.SetPath(Path);
	if (SoftObjectPath.IsValid())
		Mesh->SetStaticMesh(static_cast<UStaticMesh*>(SoftObjectPath.TryLoad()));
}

float APokerChip::GetValue() const
{
	return Value;
}

EChipColor APokerChip::GetColor() const
{
	return Color;
}

UCurveFloat* APokerChip::GetMovementCurve() const
{
	return MovementCurve;
}

float APokerChip::GetChipHeight() const
{
	return ChipHeight;
}

