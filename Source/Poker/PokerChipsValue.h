// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerChipsValue.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FPokerChipsValue
{
 GENERATED_BODY()

 UPROPERTY(EditAnywhere, BlueprintReadWrite)
 float RedChipValue;

 UPROPERTY(EditAnywhere, BlueprintReadWrite)
 float BlueChipValue;

 UPROPERTY(EditAnywhere, BlueprintReadWrite)
 float GreenChipValue;
 
 UPROPERTY(EditAnywhere, BlueprintReadWrite)
 float BlackChipValue;
};
