// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#include "Poker.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Poker, "Poker" );
