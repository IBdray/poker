// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "ChipManager.h"

#include "PokerChip.h"
#include "PokerGameModeBase.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UChipManager::UChipManager()
{
	PrimaryComponentTick.bCanEverTick = false;
	SpacingY = 4.0f;
	SpacingX = 3.0f;
	ChipsInSetMin = 15.0f;
	ChipsInSetMax = 30.0f;
	NumOfLocations = 5;

	ChipsMovementDelay = 0.1f;

	PokerChipClass = APokerChip::StaticClass();	
}


// Called when the game starts
void UChipManager::BeginPlay()
{
	Super::BeginPlay();
	
}

void UChipManager::CreateChipsSet(FVector& Location, const int32 SizeOfSet, float ValueOfSet)
{
#define CHECK_CAPACITY(Val) ValueOfSet / Val <= SizeOfSet && CurrentSize + ValueOfSet / Val <= SizeOfSet

	int32 CurrentSize = 0;
	TArray<float> ChipsAmount;
	ChipsAmount.Init(0.0f, NumOfChipTypes);
	
	while (ValueOfSet > 0)
	{
		if (CHECK_CAPACITY(ChipsValue.RedChipValue))
		{
			ChipsAmount[0] += 1; // Red
			CurrentSize += 1;
			ValueOfSet -= ChipsValue.RedChipValue;
		}
		else if (CHECK_CAPACITY(ChipsValue.BlueChipValue))
		{
			ChipsAmount[1] += 1; // Blue
			CurrentSize += 1;
			ValueOfSet -= ChipsValue.BlueChipValue;
		}
		else if (CHECK_CAPACITY(ChipsValue.GreenChipValue))
		{
			ChipsAmount[2] += 1; // Green
			CurrentSize += 1;
			ValueOfSet -= ChipsValue.GreenChipValue;
		}
		else if (CHECK_CAPACITY(ChipsValue.BlackChipValue))
		{
			ChipsAmount[3] += 1; // Black
			CurrentSize += 1;
			ValueOfSet -= ChipsValue.BlackChipValue;
		}
		else
		{
			ChipsAmount[3] = SizeOfSet; // Black
			break;
		}
	}

	NumOfChips += CurrentSize;
	for (int32 ChipType = ChipsAmount.Num()-1; ChipType >= 0; --ChipType)
		for (float i = 0.0f; i < ChipsAmount[ChipType]; ++i)
			CreateChip(static_cast<EChipColor>(ChipType), Location);
}

void UChipManager::CreateChip(const EChipColor ChipType, FVector& TopLocationOfSet)
{
	const FRotator ChipRotation(0.0f, FMath::RandRange(-180.0f, 180.0f), 0.0f);

	APokerChip* Chip = GWorld->SpawnActor<APokerChip>(PokerChipClass, TopLocationOfSet, ChipRotation);
	Chip->Init(ChipType);
	TopLocationOfSet.Z += Chip->GetChipHeight();
	
	Chips.Emplace(Chip);
}

void UChipManager::AddChips(const TArray<APokerChip*>& ChipsSet)
{
	for (auto Chip : ChipsSet)
		Chips.Emplace(Chip);
}

TArray<APokerChip*> UChipManager::TakeChipsSet(float Value)
{
	TArray<APokerChip*> ChipsSet;
	
	while (Value > 0 && Chips.Num() != 0)
	{
		APokerChip* Chip = Chips.Last();

		Chips.RemoveAt(Chips.Num()-1);
		Value -= Chip->GetValue();
		ChipsSet.Emplace(Chip);
	}
	return ChipsSet;
}

TArray<APokerChip*> UChipManager::TakeAllChips()
{
	TArray<APokerChip*> ChipsSet;
	
	for (int32 i = 0; i < Chips.Num(); ++i)
	{
		ChipsSet.Emplace(Chips[i]);
		Chips[i] = nullptr;
	}
	Chips.Empty();
	
	return ChipsSet;
}

void UChipManager::AddChipsLocation(FVector Location)
{
	ChipsLocations.Emplace(Location);
}

FVector& UChipManager::GetRandomLocation()
{
	return ChipsLocations[FMath::RandRange(0, ChipsLocations.Num()-1)];
}

float UChipManager::MoveChipsSet(FVector& Location, const TArray<APokerChip*>& ChipsSet)
{
	for (int32 i = 0; i < ChipsSet.Num(); ++i)
	{
		APokerChip* Chip = ChipsSet[i];

		FTimerHandle Timer;
		GetWorld()->GetTimerManager().SetTimer(Timer, [=]()
			{
				Chip->Move(FTransform(FRotator(0.0f, FMath::RandRange(-180.0f, 180.0f), 0.0f),Location));
			}, 0.1f, false, i * ChipsMovementDelay);
		
		Location.Z += Chip->GetChipHeight();
	}
	return (ChipsSet.Num()-1) * ChipsMovementDelay;
}

float UChipManager::MoveChipsSetRandom(const TArray<APokerChip*>& ChipsSet, bool PlaySound)
{
	for (FVector& Location : ChipsLocations)
		Location.Z = GetComponentLocation().Z;
	
	for (int32 i = 0; i < ChipsSet.Num(); ++i)
	{
		APokerChip* Chip = ChipsSet[i];
		FVector& Location = ChipsLocations[FMath::RandRange(0, ChipsLocations.Num()-1)];

		FTimerHandle Timer;
		GetWorld()->GetTimerManager().SetTimer(Timer, [=]()
			{
				Chip->Move(FTransform(FRotator(0.0f, FMath::RandRange(-180.0f, 180.0f), 0.0f),Location), PlaySound);
			}, 0.1f, false, i * ChipsMovementDelay);
		
		Location.Z += Chip->GetChipHeight();
	}
	return (ChipsSet.Num()-1) * ChipsMovementDelay;
}

void UChipManager::Init(const FTransform& Position, const float CashAmount)
{
	APokerGameModeBase* GameMode = Cast<APokerGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	ChipsValue = GameMode->GetChipsValue();
	NumOfChipTypes = GameMode->GetInitialInfo().NumOfChipTypes;
	SetWorldTransform(Position);
	
	for (int32 i = 0; i < NumOfLocations; ++i)
	{
		AddRelativeLocation(FVector((i % 2 == 0) ? SpacingX : -SpacingX, SpacingY, 0.0f));
		
		ChipsLocations.Emplace(GetComponentLocation());
		CreateChipsSet(ChipsLocations.Last(), FMath::RandRange(ChipsInSetMin, ChipsInSetMax), CashAmount / NumOfLocations);
	}
}

void UChipManager::ReInit(const float CashAmount, bool CreateChipsSets)
{
	for (auto Chip : Chips)
		Chip->Destroy();
	Chips.Empty();

	for (FVector& Location : ChipsLocations)
	{
		Location.Z = GetComponentLocation().Z;
		
		if (CreateChipsSets)
			CreateChipsSet(Location, FMath::RandRange(ChipsInSetMin, ChipsInSetMax), CashAmount / ChipsLocations.Num());
	}
}

// === Getters ===
int32 UChipManager::GetNumberOfChips() const
{
	return Chips.Num();
}

