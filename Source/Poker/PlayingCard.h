// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PlayingCardsEnums.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Actor.h"
#include "PlayingCard.generated.h"

class ADeckOfCards;
class USplineComponent;
class USoundCue;


UCLASS()
class POKER_API APlayingCard : public AActor
{
	GENERATED_BODY()
	
public:	
	APlayingCard();

	FORCEINLINE constexpr bool operator> (const APlayingCard& Other) const
	{
		return Power > Other.Power;
	}
	FORCEINLINE constexpr bool operator>= (const APlayingCard& Other) const
	{
		return Power >= Other.Power;
	}
	FORCEINLINE constexpr bool operator< (const APlayingCard& Other) const
	{
		return Power < Other.Power;
	}
	FORCEINLINE constexpr bool operator<= (const APlayingCard& Other) const
	{
		return Power <= Other.Power;
	}
	FORCEINLINE constexpr bool operator== (const APlayingCard& Other) const
	{
		return (Power == Other.Power && Suit == Other.Suit);
	}
	FORCEINLINE constexpr bool operator!= (const APlayingCard& Other) const
	{
		return (Power != Other.Power || Suit != Other.Suit);
	}

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void TimelineMovementProcess(float Value);
	UFUNCTION()
	void TimelineMovementFinish();
	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Scene;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USplineComponent* Spline;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAudioComponent* Audio;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture2D* Icon;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCurveFloat* MovementCurve;

	UPROPERTY(meta=(AllowedClasses="StaticMesh, Texture2D, CurveFloat"))
	FSoftObjectPath SoftObjectPath;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Card Info")
	ECardSuit Suit;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Card Info")
	ECardPower Power;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Card Info")
	FString MeshName;

private:
	FTimeline MovementTimeline;
	FString CharPower;

	bool bUpdateLookoutPosition;
	bool bHideAfterMovement;
	bool bDoubleMovement;

	UPROPERTY(VisibleAnywhere, Category = "Positioning")
	FTransform OnDeskPosition;
	UPROPERTY(VisibleAnywhere, Category = "Positioning")
	FTransform LookoutPosition;
	UPROPERTY(VisibleAnywhere, Category = "Positioning")
	FTransform MovementPosition;
	FTransform DoubleMovementPosition;
	

	TWeakObjectPtr<ADeckOfCards> DeckOfCards;
	
public:
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable)
	void Move(FTransform Position);
	UFUNCTION(BlueprintCallable)
	void Place(FTransform Position, bool UpdateLookoutPosition);
	UFUNCTION(BlueprintCallable)
	void RiseAndMove(FTransform Position);
	UFUNCTION(BlueprintCallable)
	void ReturnToDeck();

	UFUNCTION(BlueprintCallable)
	void CreateSpline();

	// === Sounds ===	
	UFUNCTION(BlueprintImplementableEvent)
	void OnSlide();
	UFUNCTION(BlueprintImplementableEvent)
	void OnSlideAndPlace();
	UFUNCTION(BlueprintImplementableEvent)
	void OnShovel();
	UFUNCTION(BlueprintImplementableEvent)
	void OnOpen();

	/*
Calculates and saves Position of the end of the spline component
	 */
	UFUNCTION(BlueprintCallable)
	void UpdateLookoutPosition();

	UFUNCTION(BlueprintCallable)
	FTransform GetOnDeskPosition() const;
	UFUNCTION(BlueprintCallable)
	FTransform GetLookoutPosition() const;


	UFUNCTION(BlueprintCallable)
	ECardSuit GetSuit() const;
	UFUNCTION(BlueprintCallable)
	ECardPower GetPower() const;
	UFUNCTION(BlueprintCallable)
	FString GetCharPower() const;
	UFUNCTION(BlueprintCallable)
	FString GetMeshName() const;

	UFUNCTION(BlueprintCallable)
	UStaticMeshComponent* GetMesh() const;
	UFUNCTION(BlueprintCallable)
	UTexture2D* GetImage() const;
	UFUNCTION(BlueprintCallable)
	UCurveFloat* GetMovementCurve() const;

	UFUNCTION(BlueprintCallable)
	void ShowCard();
	UFUNCTION(BlueprintCallable)
	void HideCard();

	void SetDeckOfCards(ADeckOfCards* Deck);
	
	void Init(ECardSuit CardSuit, ECardPower CardPower, const FString& CardName);
};
