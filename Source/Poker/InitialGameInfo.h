// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "InitialGameInfo.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FInitialGameInfo
{
 GENERATED_BODY()
 
 FInitialGameInfo();
 
 int32 NumOfCardsHeldByPlayers;
 int32 NumOfCommunityCards;
 float WalletInitialCash;
 float PotInitialCash;
 float SmallBlindValue;
 float BigBlindValue;
 float MinRaiseAmount;
 int32 NumOfChipTypes;
};
