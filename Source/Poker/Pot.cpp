// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "Pot.h"

#include "CashManager.h"
#include "ChipManager.h"
#include "PokerGameModeBase.h"
#include "Kismet/GameplayStatics.h"


APot::APot()
{
	PrimaryActorTick.bCanEverTick = false;

	ChipsSetLocations.Emplace(FVector(-167.9f, -368.2f, 95.8f));

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);

	Cash = CreateDefaultSubobject<UCashManager>(TEXT("Cash"));
}

// Called when the game starts or when spawned
void APot::BeginPlay()
{
	Super::BeginPlay();
}

void APot::PrepareForNewMatch()
{
	Cash->GetChipManager()->ReInit(0.0f, false);
}

void APot::SetPosition(const FTransform& Position)
{
	SetActorTransform(Position);
	Cash->GetChipManager()->SetWorldTransform(Position);
}

void APot::AddCash(const float Amount)
{
	Cash->AddCash(Amount);
}

float APot::TakeAllCash()
{
	return Cash->TakeCash(Cash->GetCash());
}

float APot::AddChips(const TArray<APokerChip*>& ChipsSet)
{
	Cash->GetChipManager()->AddChips(ChipsSet);
	return Cash->GetChipManager()->MoveChipsSet(Cash->GetChipManager()->GetRandomLocation(), ChipsSet);
}

TArray<APokerChip*> APot::TakeAllChips()
{
	return Cash->GetChipManager()->TakeAllChips();
}

float APot::GetCash() const
{
	return Cash->GetCash();
}

void APot::Init()
{
	APokerGameModeBase* GameMode = Cast<APokerGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	Cash->SetInitialCash(GameMode->GetInitialInfo().PotInitialCash);
}

void APot::ResetDefault()
{
	Init();
	Cash->GetChipManager()->ReInit(0.0f, false);
}

