// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.h"
#include "InitialGameInfo.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

class APlayingCard;
class APokerGameModeBase;
class UHandManager;
class UCashManager;
class UTurnStateMachine;
class UActionConditionManager;
class UArrowComponent;
class UCameraComponent;

class APokerChip;


UCLASS()
class POKER_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void AddControllerPitchInput(float Val) override;
	virtual void AddControllerYawInput(float Val) override;

	bool CheckCanRaise(const float Amount) const;

	UFUNCTION(BlueprintCallable)
	void StartIncreaseRaise();
	UFUNCTION(BlueprintCallable)
	void StopIncreaseRaise();
	UFUNCTION(BlueprintCallable)
	void StartReduceRaise();
	UFUNCTION(BlueprintCallable)
	void StopReduceRaise();
	
	UPROPERTY()
	APokerGameModeBase* GameMode;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* Scene;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* HandOnDeskPosition;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* CommunityHandPosition;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* DeckOfCardsPosition;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* WhiteChipPosition;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* WalletPosition;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UHandManager* HandOfCards;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCashManager* Wallet;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UTurnStateMachine* TurnStateMachine;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UActionConditionManager* ActionCondition;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString PlayerName;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bWhiteChipOwner;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bFolded;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bAllIn;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bGaveBigBlind;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bLookingAtCards;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float DefaultRaiseRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float DefaultRaiseRateMultiplier;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float RateOfRaiseRateTimer;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bRaiseActive;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Bet;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float RaiseAmount;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float RaiseAmountBackup;
	
	
	// === Delay Properties ===
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Delay Properties")
	float EndTurnDelay;

	// === Class Defaults ===
	UPROPERTY(EditAnywhere, Category = "Class Defaults")
	TSubclassOf<APokerChip> PokerChipClass;

	// === Control Settings ===
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MousePitchMultiplier;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouseYawMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bInvertVertically;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bInvertHorizontally;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bToggleLookAtCards;
	
private:
	FInitialGameInfo InitialGameInfo;
	
	FTimerHandle IncreaseRaiseTimer;
	FTimerHandle ReduceRaiseTimer;
	float RaiseRate;

	FTimerHandle IncreaseRaiseRateTimer;
	FTimerHandle ReduceRaiseRateTimer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void PrepareToNewMatch();
	UFUNCTION(BlueprintCallable)
	void TakeTurn();
	UFUNCTION(BlueprintCallable)
	void EndTurn();
	
	UFUNCTION(BlueprintCallable)
	void Call();
	UFUNCTION(BlueprintCallable)
	void Raise();
	UFUNCTION(BlueprintCallable)
	void RaiseByAmount(float Amount);
	UFUNCTION(BlueprintCallable)
	void Fold();
	
	UFUNCTION(BlueprintCallable)
	void SubmitRaise();
	UFUNCTION(BlueprintCallable)
	void CancelRaise();

	UFUNCTION(BlueprintCallable)
	void BigBlind();
	UFUNCTION(BlueprintCallable)
	void SmallBlind();

	UFUNCTION(BlueprintCallable)
	void AddCard(APlayingCard* Card);
	UFUNCTION(BlueprintCallable)
	void EmptyHand();
	UFUNCTION(BlueprintCallable)
	void PickHandUp();
	UFUNCTION(BlueprintCallable)
	void PutHandDown();
	UFUNCTION(BlueprintCallable)
	void ToggleLookAtCards();

	// === Cash Operations ===
	/*
Calculates Amount Of Cash To Take
Also Saves Amount Of Cash To Raise And Adds It To Bet
 
Returns: Raise Amount
	 */
	UFUNCTION(BlueprintCallable)
	float TakeFromWallet(const float Amount);
	UFUNCTION(BlueprintCallable)
	void AddToWallet(const float Amount);
	UFUNCTION(BlueprintCallable)
	void AddCashToPot(const float Amount);

	UFUNCTION(BlueprintCallable)
	void AddNewCash(const float Amount);

	UFUNCTION(BlueprintCallable)
	bool CheckCanReduceRaiseAmount() const;
	UFUNCTION(BlueprintCallable)
	bool CheckCanIncreaseRaiseAmount() const;
	UFUNCTION(BlueprintCallable)
	bool CheckEnoughBet() const;
	UFUNCTION(BlueprintCallable)
	void ChangeActionWithAllInCheck(EPlayerActions Action);
	
	UFUNCTION(BlueprintCallable)
	UHandManager* GetHand() const;
	UFUNCTION(BlueprintCallable)
	UCashManager* GetWallet() const;
	UFUNCTION(BlueprintCallable)
	UTurnStateMachine* GetTurnStateMachine() const;

	UFUNCTION(BlueprintCallable)
	bool GetWhiteChipOwner() const;
	UFUNCTION(BlueprintCallable)
	void SetWhiteChipOwner(const bool Val);
	UFUNCTION(BlueprintCallable)
	FVector GetWhiteChipPosition() const;

	UFUNCTION(BlueprintCallable)
	FTransform GetCommunityHandPosition() const;
	UFUNCTION(BlueprintCallable)
	FTransform GetDeckOfCardsPosition() const;

	UFUNCTION(BlueprintCallable)
	EPlayerActions GetTurnAction() const;
	UFUNCTION(BlueprintCallable)
	EPlayerTurnState GetTurnState() const;
	UFUNCTION(BlueprintCallable)
	float GetBet() const;
	UFUNCTION(BlueprintCallable)
	float GetRaiseAmount() const;
	UFUNCTION(BlueprintCallable)
	FString GetPlayerName() const;
	UFUNCTION(BlueprintCallable)
	bool GetFolded() const;
	UFUNCTION(BlueprintCallable)
	bool GetAllIn() const;
	
	UFUNCTION(BlueprintCallable)
	void Init();
	UFUNCTION(BlueprintCallable)
	void ResetDefault();
	
	// === Events ===
	UFUNCTION(BlueprintImplementableEvent)
	void OnRaise();
	UFUNCTION(BlueprintImplementableEvent)
    void OnSubmitRaise();
	UFUNCTION(BlueprintImplementableEvent)
	void OnCancelRaise();

	UFUNCTION(BlueprintImplementableEvent)
	void WarningRaiseCap();
	UFUNCTION(BlueprintImplementableEvent)
	void WarningNotActive();
	UFUNCTION(BlueprintImplementableEvent)
	void WarningFolded();
	UFUNCTION(BlueprintImplementableEvent)
	void WarningNotEnoughCash();
	UFUNCTION(BlueprintImplementableEvent)
	void WarningMinBet();
};
