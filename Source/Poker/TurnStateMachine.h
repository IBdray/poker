// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.h"
#include "Components/ActorComponent.h"
#include "TurnStateMachine.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class POKER_API UTurnStateMachine : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTurnStateMachine();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EPlayerTurnState TurnState;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EPlayerActions TurnAction;

public:	
	UFUNCTION(BlueprintCallable)
	bool ChangeState(const EPlayerTurnState State);
	UFUNCTION(BlueprintCallable)
	bool ChangeAction(const EPlayerActions Action);
	UFUNCTION(BlueprintCallable)
	void ResetTurn();
	UFUNCTION(BlueprintCallable)
	void ResetStates();
	
	UFUNCTION(BlueprintCallable)
	bool CheckActiveMove() const;
	UFUNCTION(BlueprintCallable)
	bool CheckMadeMove() const;
	UFUNCTION(BlueprintCallable)
	bool CheckNotMadeMove() const;

	UFUNCTION(BlueprintCallable)
	bool CheckCanMove() const;
	
	UFUNCTION(BlueprintCallable)
	EPlayerTurnState GetCurrentState() const;
	UFUNCTION(BlueprintCallable)
	EPlayerActions GetCurrentAction() const;
		
};
