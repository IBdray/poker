// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "PlayingCard.h"

#include "DeckOfCards.h"
#include "Components/AudioComponent.h"
#include "Components/SplineComponent.h"
#include "Runtime/CoreUObject/Public/UObject/SoftObjectPtr.h"

APlayingCard::APlayingCard()
{
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);

	Spline = CreateDefaultSubobject<USplineComponent>(TEXT("Spline"));
	Spline->SetupAttachment(Scene);
	Spline->ClearSplinePoints();

	Audio = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));
	Audio->SetupAttachment(Scene);
	Audio->bAutoActivate = false;
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Scene);
	Mesh->SetHiddenInGame(true);
}

// Called when the game starts or when spawned
void APlayingCard::BeginPlay()
{
	Super::BeginPlay();

	SoftObjectPath.SetPath(TEXT("CurveFloat'/Game/Poker/PokerAssets/DeckOfCards/CF_CardMovement.CF_CardMovement'"));
	if (SoftObjectPath.IsValid())
		MovementCurve = Cast<UCurveFloat>(SoftObjectPath.TryLoad());

	FOnTimelineFloat ProgressFunction;
	ProgressFunction.BindUFunction(this, TEXT("TimelineMovementProcess"));
	MovementTimeline.AddInterpFloat(MovementCurve, ProgressFunction);

	FOnTimelineEvent OnTimelineFinishedFunction;
	OnTimelineFinishedFunction.BindUFunction(this, TEXT("TimelineMovementFinish"));
	MovementTimeline.SetTimelineFinishedFunc(OnTimelineFinishedFunction);

	MovementTimeline.SetTimelineLengthMode(TL_LastKeyFrame);
}

void APlayingCard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MovementTimeline.IsPlaying())
		MovementTimeline.TickTimeline(DeltaTime);
}

void APlayingCard::Move(FTransform Position)
{
	MovementPosition = Position;
	MovementTimeline.PlayFromStart();
}

void APlayingCard::Place(FTransform Position, bool UpdateLookoutPosition)
{
	bUpdateLookoutPosition = UpdateLookoutPosition;
	OnDeskPosition = Position;
	ShowCard();
	
	Move(Position);
}

void APlayingCard::RiseAndMove(FTransform Position)
{
	bDoubleMovement = true;
	DoubleMovementPosition = Position;
	
	FVector RiseLocation(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 15.0f);
	Move(FTransform(Position.Rotator(), RiseLocation));
}

void APlayingCard::ReturnToDeck()
{
	OnDeskPosition = FTransform();
	LookoutPosition = FTransform();
	Spline->ClearSplinePoints();

	bHideAfterMovement = true;
	DeckOfCards->ReturnCard(this);
}

void APlayingCard::CreateSpline()
{
	Spline->AddSplineLocalPoint(FVector::ZeroVector);
	Spline->AddSplineLocalPoint(FVector(0.0f, -35.0f, 50.0f));
	Spline->SetRotationAtSplinePoint(1, FRotator(0.0f, 0.0f, -109.6f), ESplineCoordinateSpace::Local);
}

void APlayingCard::UpdateLookoutPosition()
{
	FVector SplineEndLocation = Spline->GetLocationAtDistanceAlongSpline(Spline->GetSplineLength(), ESplineCoordinateSpace::World);
	FRotator SplineEndRotation = Spline->GetRotationAtDistanceAlongSpline(Spline->GetSplineLength(), ESplineCoordinateSpace::World);

	LookoutPosition = FTransform(SplineEndRotation, SplineEndLocation);
}

// === Timeline Methods ===
void APlayingCard::TimelineMovementProcess(float Value)
{
	FVector Location = FMath::Lerp(GetActorLocation(), MovementPosition.GetLocation(), Value);
	FRotator Rotation = FMath::Lerp<FRotator>(GetActorRotation(), MovementPosition.Rotator(), Value);
	SetActorLocationAndRotation(Location, Rotation);
}

void APlayingCard::TimelineMovementFinish()
{
	if (bUpdateLookoutPosition && !bDoubleMovement)
	{
		bUpdateLookoutPosition = false;
		UpdateLookoutPosition();
	}
	if (bHideAfterMovement && !bDoubleMovement)
	{
		bHideAfterMovement = false;
		HideCard();
	}
	if (bDoubleMovement)
	{
		bDoubleMovement = false;
		Move(DoubleMovementPosition);
	}
}

void APlayingCard::ShowCard()
{
	Mesh->SetHiddenInGame(false);
}

void APlayingCard::HideCard()
{
	Mesh->SetHiddenInGame(true);
}


void APlayingCard::Init(ECardSuit CardSuit, ECardPower CardPower, const FString& CardName)
{
	Suit = CardSuit;
	Power = CardPower;
	MeshName = CardName;
	const FString CardPath = "StaticMesh'/Game/Poker/PokerAssets/DeckOfCards/StaticMeshes/SM_" + CardName + ".SM_" + CardName + "'";
	const FString ImagePath = "Texture2D'/Game/Poker/PokerAssets/DeckOfCards/Textures/CardsFaces_Basic/T_Icon_" + CardName + ".T_Icon_" + CardName + "'";

	SoftObjectPath.SetPath(CardPath);
	if (SoftObjectPath.IsValid())
		Mesh->SetStaticMesh(Cast<UStaticMesh>(SoftObjectPath.TryLoad()));

	SoftObjectPath.SetPath(ImagePath);
	if (SoftObjectPath.IsValid())
		Icon = Cast<UTexture2D>(SoftObjectPath.TryLoad());

	CharPower = FString::Chr("23456789ABCDE"[static_cast<int32>(CardPower)]);
}

void APlayingCard::SetDeckOfCards(ADeckOfCards* Deck)
{
	DeckOfCards = Deck;
}

// === Getters ===
ECardSuit APlayingCard::GetSuit() const
{
	return Suit;
}
ECardPower APlayingCard::GetPower() const
{
	return Power;
}
FString APlayingCard::GetCharPower() const
{
	return CharPower;
}
FString APlayingCard::GetMeshName() const
{
	return MeshName;
}

FTransform APlayingCard::GetOnDeskPosition() const
{
	return OnDeskPosition;
}
FTransform APlayingCard::GetLookoutPosition() const
{
	return LookoutPosition;
}

UStaticMeshComponent* APlayingCard::GetMesh() const
{
	return Mesh;
}
UTexture2D* APlayingCard::GetImage() const
{
	return Icon;
}
UCurveFloat* APlayingCard::GetMovementCurve() const
{
	return MovementCurve;
}
