// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.h"
#include "GameFramework/Actor.h"
#include "PlayersManager.generated.h"

class APlayerPawn;
class APokerGameModeBase;


UCLASS()
class POKER_API APlayersManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayersManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AActor*> Players;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 ActivePlayerNum;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	APlayerPawn* ActivePlayer;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 HostNum;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	APlayerPawn* Host;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bAllPlayersFolded;

	// === Delay Properties ===
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Delay Properties")
	float ResetPlayerDelay;

private:
	UPROPERTY()
	APokerGameModeBase* GameMode;
	
public:	
	UFUNCTION(BlueprintCallable)
	APlayerPawn* SetNewHost();

	UFUNCTION(BlueprintCallable)
	void PrepareToNewMatch();	

	UFUNCTION(BlueprintCallable)
	APlayerPawn* NextTurn();
	
	UFUNCTION(BlueprintCallable)
	ETurnResults CheckTurnResults();
	UFUNCTION(BlueprintCallable)
	void ResetPlayersTurnStates();
	UFUNCTION(BlueprintCallable)
	float ResetPlayersMatchStates();

	UFUNCTION(BlueprintCallable)
	void RankPlayersHands();
	UFUNCTION(BlueprintCallable)
	void OpenPlayersHands(); 

	UFUNCTION(BlueprintCallable)
	APlayerPawn* NextPlayer();
	UFUNCTION(BlueprintCallable)
	APlayerPawn* CurrentPlayer() const;
	UFUNCTION(BlueprintCallable)
	APlayerPawn* PreviousPlayer();
	
	UFUNCTION(BlueprintCallable)
	int32 NextPlayerNum();
	UFUNCTION(BlueprintCallable)
	int32 CurrentPlayerNum() const;
	UFUNCTION(BlueprintCallable)
	int32 PreviousPlayerNum();

	UFUNCTION(BlueprintCallable)
	int32 GetNumberOfPlayers() const;
	UFUNCTION(BlueprintCallable)
	TArray<AActor*> GetPlayers() const;
	
	UFUNCTION(BlueprintCallable)
	TArray<APlayerPawn*> GetAlivePlayers() const;
	UFUNCTION(BlueprintCallable)
	bool CheckAllPlayersFolded() const;
	UFUNCTION(BlueprintCallable)
	float GetMinCash() const;

	UFUNCTION(BlueprintCallable)
	void Init();
	UFUNCTION(BlueprintCallable)
	void ResetDefault();
};
