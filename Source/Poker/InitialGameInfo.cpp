// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "InitialGameInfo.h"

FInitialGameInfo::FInitialGameInfo()
{
	NumOfCardsHeldByPlayers = 2;
	NumOfCommunityCards = 5;
	WalletInitialCash = 500.0f;
	PotInitialCash = 0.0f;
	SmallBlindValue = 5.0f;
	BigBlindValue = 10.0f;
	MinRaiseAmount = 5.0f;
	NumOfChipTypes = 4;
}
