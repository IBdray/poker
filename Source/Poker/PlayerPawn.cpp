// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "PlayerPawn.h"

#include "ActionConditionManager.h"
#include "CashManager.h"
#include "ChipManager.h"
#include "HandManager.h"
#include "TurnStateMachine.h"
#include "Camera/CameraComponent.h"
#include "Components/ArrowComponent.h"
#include "PokerGameModeBase.h"
#include "PlayingCard.h"
#include "PokerChip.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PlayerName = "AI Player";

	DefaultRaiseRate = 0.3f;
	DefaultRaiseRateMultiplier = 0.9f;
	RateOfRaiseRateTimer = 0.7f;
	RaiseRate = DefaultRaiseRate;

	// === Delay Properties ===
	EndTurnDelay = 1.0f;

	// === Class Defaults ===
	PokerChipClass = APokerChip::StaticClass();

	// === Control Settings ===
	bToggleLookAtCards = true;

	MousePitchMultiplier = 1.0f;
	MouseYawMultiplier = 1.0f;
	bInvertVertically = false;
	bInvertHorizontally = false;
	

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	Scene->SetupAttachment(RootComponent);
	
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(Scene);
	Camera->SetRelativeRotation(FRotator(-10.0f, 0.0f, 0.0f));

	HandOnDeskPosition = CreateDefaultSubobject<UArrowComponent>(TEXT("Hand On Desk Position"));
	HandOnDeskPosition->SetupAttachment(Scene);
	HandOnDeskPosition->ArrowColor = FColor::Green;
	HandOnDeskPosition->SetRelativeLocationAndRotation(FVector(86.3f,-4.8f,-54.0f),
													   FRotator(0.0f,94.0f,90.0f));

	CommunityHandPosition = CreateDefaultSubobject<UArrowComponent>(TEXT("Community Hand Position"));
	CommunityHandPosition->SetupAttachment(Scene);
	CommunityHandPosition->ArrowColor = FColor::Blue;
	CommunityHandPosition->ArrowLength = 20.0f;
	CommunityHandPosition->SetRelativeLocationAndRotation(FVector(103.4f, -29.2f, -54.0f), 
														  FRotator(0.0f, 92.3f, -90.0f));

	DeckOfCardsPosition = CreateDefaultSubobject<UArrowComponent>(TEXT("Deck Of Cards Position"));
	DeckOfCardsPosition->SetupAttachment(Scene);
	DeckOfCardsPosition->ArrowColor = FColor::Orange;
	DeckOfCardsPosition->ArrowLength = 20.0f;
	DeckOfCardsPosition->SetRelativeLocationAndRotation(FVector(82.6f, -22.8f, -54.0f),
														FRotator(0.0f, 52.3f, 90.0f));

	
	WhiteChipPosition = CreateDefaultSubobject<UArrowComponent>(TEXT("White Chip Position"));
	WhiteChipPosition->SetupAttachment(Scene);
	WhiteChipPosition->ArrowColor = FColor::White;
	WhiteChipPosition->ArrowLength = 30.0f;
	WhiteChipPosition->SetRelativeLocation(FVector(79.3f,21.1f,-54.0f));

	WalletPosition = CreateDefaultSubobject<UArrowComponent>(TEXT("Wallet Position"));
	WalletPosition->SetupAttachment(Scene);
	WalletPosition->ArrowColor = FColor::Cyan;
	WalletPosition->ArrowLength = 30.0f;
	WalletPosition->SetRelativeLocation(FVector(91.8f,39.6f,-54.0f));

	
	HandOfCards = CreateDefaultSubobject<UHandManager>(TEXT("Hand Of Cards"));
	Wallet = CreateDefaultSubobject<UCashManager>(TEXT("Wallet"));
	Wallet->SetupAttachment(Scene);
	
	TurnStateMachine = CreateDefaultSubobject<UTurnStateMachine>(TEXT("Turn State"));
	ActionCondition = CreateDefaultSubobject<UActionConditionManager>(TEXT("Action Condition"));
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Call", IE_Pressed, this, &APlayerPawn::Call);
	PlayerInputComponent->BindAction("Raise", IE_Pressed, this, &APlayerPawn::Raise);
	PlayerInputComponent->BindAction("Fold", IE_Pressed, this, &APlayerPawn::Fold);
	
	PlayerInputComponent->BindAction("LookOnCards", IE_Pressed, this, &APlayerPawn::ToggleLookAtCards);
	PlayerInputComponent->BindAction("LookOnCards", IE_Released, this, &APlayerPawn::PutHandDown);

	PlayerInputComponent->BindAction("IncreaseBet", IE_Pressed, this, &APlayerPawn::StartIncreaseRaise);
	PlayerInputComponent->BindAction("IncreaseBet", IE_Released, this, &APlayerPawn::StopIncreaseRaise);
	PlayerInputComponent->BindAction("ReduceBet", IE_Pressed, this, &APlayerPawn::StartReduceRaise);
	PlayerInputComponent->BindAction("ReduceBet", IE_Released, this, &APlayerPawn::StopReduceRaise);
	PlayerInputComponent->BindAction("SubmitRaise", IE_Pressed, this, &APlayerPawn::SubmitRaise);

	PlayerInputComponent->BindAxis("LookUp", this, &APlayerPawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &APlayerPawn::AddControllerYawInput);
}

void APlayerPawn::PrepareToNewMatch()
{
	//Check what will happen if change will occur in menu 
	ActionCondition->SetCondition(EPlayerActionCondition::PAC_DefaultGame);
	
	if (Wallet->GetCash() < InitialGameInfo.BigBlindValue)
		Wallet->AddCash(500.0f);
	
	Wallet->UpdateInitialCash();
	Wallet->GetChipManager()->ReInit(Wallet->GetCash());
	HandOfCards->HideHand();
	
	bFolded = false;
	bAllIn = false;
	bRaiseActive = false;
	bGaveBigBlind = false;
	
	Bet = 0.0f;
	RaiseAmount = 0.0f;
	RaiseAmountBackup = 0.0f;
}

void APlayerPawn::TakeTurn()
{
	if (!CheckEnoughBet() && !bAllIn)
		TurnStateMachine->ChangeState(EPlayerTurnState::PTS_NotMadeMove);
	if (bGaveBigBlind)
	{
		bGaveBigBlind = false;
		TurnStateMachine->ChangeState(EPlayerTurnState::PTS_NotMadeMove);
	}

	if (bFolded || bAllIn)
		EndTurn();
	else
		TurnStateMachine->ChangeState(EPlayerTurnState::PTS_ActiveMove);
}

void APlayerPawn::EndTurn()
{
	TurnStateMachine->ChangeState(EPlayerTurnState::PTS_MadeMove);

	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [=]()
	{
		GameMode->Turn();
	}, 0.1f, false, Wallet->GetChipsMovementDelay() + EndTurnDelay);
}

// === Control ===
void APlayerPawn::Call()
{
	if (GameMode->GetGameStarted() && TurnStateMachine->CheckCanMove() && ActionCondition->CheckDefaultCondition())
	{
		if (!CheckEnoughBet())
		{
			AddCashToPot(GameMode->GetLastBet() - Bet);
			ChangeActionWithAllInCheck(EPlayerActions::PA_Call);
		}
		else
			TurnStateMachine->ChangeAction(EPlayerActions::PA_Check);

		EndTurn();
	}
	else
	{
		if (bFolded)
			WarningFolded();
		else if (!TurnStateMachine->CheckActiveMove())
			WarningNotActive();
	}
}

void APlayerPawn::Raise()
{
	if (GameMode->GetGameStarted() && TurnStateMachine->CheckCanMove() && ActionCondition->CheckDefaultCondition() && GameMode->CheckCanRaise())
	{
		ActionCondition->SetCondition(EPlayerActionCondition::PAC_RaiseBet);
		bRaiseActive = true;

		RaiseAmountBackup = RaiseAmount;
		RaiseAmount = (CheckCanRaise(GameMode->GetMinRaise())) ? GameMode->GetMinRaise() : Wallet->GetCash();
		
		OnRaise();
	}
	else
	{
		if (bFolded)
			WarningFolded();
		else if (!TurnStateMachine->CheckActiveMove())
			WarningNotActive();
		else if (!GameMode->CheckCanRaise())
			WarningRaiseCap();
	}
}

void APlayerPawn::RaiseByAmount(float Amount)
{
	if (GameMode->GetGameStarted() && TurnStateMachine->CheckCanMove() && ActionCondition->CheckDefaultCondition() && GameMode->CheckCanRaise())
	{
		RaiseAmount = (CheckCanRaise(Amount)) ? Amount : Wallet->GetCash();
		ChangeActionWithAllInCheck(EPlayerActions::PA_Raise);

		AddCashToPot(RaiseAmount);
		GameMode->IncrementRaiseCounter();

		OnRaise();
		OnSubmitRaise();
		
		EndTurn();
	}
	else
	{
		if (bFolded)
			WarningFolded();
		else if (!TurnStateMachine->CheckActiveMove())
			WarningNotActive();
		else if (!GameMode->CheckCanRaise())
			WarningRaiseCap();
	}
}

void APlayerPawn::Fold()
{
	if (GameMode->GetGameStarted() && TurnStateMachine->CheckCanMove() && ActionCondition->CheckDefaultCondition())
	{
		TurnStateMachine->ChangeAction(EPlayerActions::PA_Fold);
		bFolded = true;

		Wallet->SetChipsMovementDelay(0.0f);
		HandOfCards->ReturnCardsToDeck();
		EndTurn();
	}
	else
	{
		if (bFolded)
			WarningFolded();
		else if (!TurnStateMachine->CheckActiveMove())
			WarningNotActive();
	}
}

void APlayerPawn::StartIncreaseRaise()
{
	if (GameMode->GetGameStarted() && bRaiseActive && ActionCondition->CheckRaiseCondition() && CheckCanIncreaseRaiseAmount())
	{
		GetWorldTimerManager().SetTimer(IncreaseRaiseRateTimer, [&]()
		{
			if (GetWorldTimerManager().TimerExists(IncreaseRaiseTimer))
			{
				GetWorldTimerManager().ClearTimer(IncreaseRaiseTimer);
				RaiseRate *= DefaultRaiseRateMultiplier;
			}

			GetWorldTimerManager().SetTimer(IncreaseRaiseTimer, [&]()
			{
				if (CheckCanIncreaseRaiseAmount())
					RaiseAmount += InitialGameInfo.MinRaiseAmount;
			}, RaiseRate, true, 0.0f);
		}, RateOfRaiseRateTimer, true, 0.0f);
	}
	else if (!CheckCanIncreaseRaiseAmount())
		WarningNotEnoughCash();
}

void APlayerPawn::StopIncreaseRaise()
{
	if (GetWorldTimerManager().TimerExists(IncreaseRaiseRateTimer) && GetWorldTimerManager().TimerExists(IncreaseRaiseTimer))
	{
		GetWorldTimerManager().ClearTimer(IncreaseRaiseRateTimer);
		GetWorldTimerManager().ClearTimer(IncreaseRaiseTimer);
		RaiseRate = DefaultRaiseRate;
	}
}

void APlayerPawn::StartReduceRaise()
{
	if (GameMode->GetGameStarted() && bRaiseActive && ActionCondition->CheckRaiseCondition() && CheckCanReduceRaiseAmount())
	{
		GetWorldTimerManager().SetTimer(ReduceRaiseRateTimer, [&]()
		{
			if (GetWorldTimerManager().TimerExists(ReduceRaiseTimer))
			{
				GetWorldTimerManager().ClearTimer(ReduceRaiseTimer);
				RaiseRate *= DefaultRaiseRateMultiplier;
			}
			
			GetWorldTimerManager().SetTimer(ReduceRaiseTimer, [&]()
			{
				if (CheckCanReduceRaiseAmount())
					RaiseAmount -= InitialGameInfo.MinRaiseAmount;
			}, RaiseRate, true, 0.0f);
		}, RateOfRaiseRateTimer, true, 0.0f);
	}
	else if (!CheckCanReduceRaiseAmount())
		WarningMinBet();
}
void APlayerPawn::StopReduceRaise()
{
	if (GetWorldTimerManager().TimerExists(ReduceRaiseRateTimer) && GetWorldTimerManager().TimerExists(ReduceRaiseTimer))
	{
		GetWorldTimerManager().ClearTimer(ReduceRaiseRateTimer);
		GetWorldTimerManager().ClearTimer(ReduceRaiseTimer);
		RaiseRate = DefaultRaiseRate;
	}
}

void APlayerPawn::SubmitRaise()
{
	if (GameMode->GetGameStarted() && bRaiseActive && ActionCondition->CheckRaiseCondition())
	{
		ActionCondition->SetCondition(EPlayerActionCondition::PAC_DefaultGame);
		bRaiseActive = false;
		ChangeActionWithAllInCheck(EPlayerActions::PA_Raise);

		AddCashToPot(RaiseAmount);
		GameMode->IncrementRaiseCounter();

		OnSubmitRaise();
		EndTurn();
	}
}

void APlayerPawn::CancelRaise()
{
	if (GameMode->GetGameStarted() && bRaiseActive && ActionCondition->CheckRaiseCondition())
	{
		ActionCondition->SetCondition(EPlayerActionCondition::PAC_DefaultGame);
		bRaiseActive = false;
		RaiseAmount = RaiseAmountBackup;
		OnCancelRaise();
	}
}

void APlayerPawn::BigBlind()
{
	if (GameMode->GetGameStarted() && TurnStateMachine->CheckCanMove())
	{
		ChangeActionWithAllInCheck(EPlayerActions::PA_BigBlind);
		bGaveBigBlind = true;
		
		AddCashToPot(InitialGameInfo.BigBlindValue);
		GameMode->IncrementRaiseCounter();
		
		EndTurn();
	}
}
void APlayerPawn::SmallBlind()
{
	if (GameMode->GetGameStarted() && TurnStateMachine->CheckCanMove())
	{
		ChangeActionWithAllInCheck(EPlayerActions::PA_SmallBlind);
		AddCashToPot(InitialGameInfo.SmallBlindValue);
		
		EndTurn();
	}
}

// === Hand Of Cards Operations ===
void APlayerPawn::AddCard(APlayingCard* Card)
{
	HandOfCards->AddToHand(Card);
	Card->OnSlide();
	Card->CreateSpline();
}

void APlayerPawn::EmptyHand()
{
	bLookingAtCards = false;
	HandOfCards->ReturnCardsToDeck();
}

float APlayerPawn::TakeFromWallet(const float Amount)
{
	RaiseAmount = Wallet->PlaceBet(Amount);
	Bet += RaiseAmount;
	
	GameMode->SetMinRaise(RaiseAmount);
	GameMode->SetLastBet(Bet);

	return RaiseAmount;
}
void APlayerPawn::AddToWallet(const float Amount)
{
	Wallet->AddCash(Amount);
}

// Adds To Pot Certain Amount Of Cash And Sets Minimum Bet Amount
// Also Saves Used Amount Of Cash In TempBet And Updates Current Bet Amount
void APlayerPawn::AddCashToPot(const float Amount)
{
	GameMode->AddToPot(TakeFromWallet(Amount));
}

bool APlayerPawn::CheckCanIncreaseRaiseAmount() const
{
	return CheckCanRaise(RaiseAmount + InitialGameInfo.MinRaiseAmount);
}
bool APlayerPawn::CheckCanReduceRaiseAmount() const
{
	return RaiseAmount > GameMode->GetMinRaise();
}

bool APlayerPawn::CheckEnoughBet() const
{
	return GameMode->GetLastBet() == Bet;
}

// Changes Player Action To All In If Wagered All Cash
// Or To Specified Action
void APlayerPawn::ChangeActionWithAllInCheck(EPlayerActions Action)
{
	if (Wallet->CheckAllIn(RaiseAmount + Bet))
	{
		TurnStateMachine->ChangeAction(EPlayerActions::PA_AllIn);
		bAllIn = true;
	}
	else
		TurnStateMachine->ChangeAction(Action);
}
bool APlayerPawn::CheckCanRaise(const float Amount) const
{
	return (Wallet->GetCash() - Amount >= 0.0f);
}

void APlayerPawn::PickHandUp()
{
	if (HandOfCards->CheckEnoughCards())
	{
		bLookingAtCards = true;
		for (APlayingCard* Card : HandOfCards->GetCardsArray())
			Card->Move(Card->GetLookoutPosition());
	}
}
void APlayerPawn::PutHandDown()
{
	if (!bToggleLookAtCards && HandOfCards->CheckEnoughCards())
	{
		bLookingAtCards = false;
		for (APlayingCard* Card : HandOfCards->GetCardsArray())
			Card->Move(Card->GetOnDeskPosition());
	}
}

void APlayerPawn::ToggleLookAtCards()
{
	if (bLookingAtCards && HandOfCards->CheckEnoughCards())
	{
		bLookingAtCards = false;
		for (APlayingCard* Card : HandOfCards->GetCardsArray())
			Card->Move(Card->GetOnDeskPosition());
	}
	else
		PickHandUp();
}

void APlayerPawn::AddNewCash(const float Amount)
{
	Wallet->AddCash(Amount);
	Wallet->UpdateInitialCash();
	Wallet->GetChipManager()->ReInit(Wallet->GetCash());
}

// === Camera ===
void APlayerPawn::AddControllerPitchInput(float Val)
{
	if (Val != 0.f && Controller && Controller->IsLocalPlayerController())
	{
		APlayerController* const PC = Cast<APlayerController>(Controller);

		float PitchInput = MousePitchMultiplier * Val;
		if (bInvertVertically)
			PitchInput *= -1.0f;
		
		PC->AddPitchInput(PitchInput);
	}
}
void APlayerPawn::AddControllerYawInput(float Val)
{
	if (Val != 0.f && Controller && Controller->IsLocalPlayerController())
	{
		APlayerController* const PC = Cast<APlayerController>(Controller);

		float YawInput = MouseYawMultiplier * Val;
		if (bInvertHorizontally)
			YawInput *= -1.0f;
		
		PC->AddYawInput(YawInput);
	}
}

// === Setters Of Member Objects ===
void APlayerPawn::SetWhiteChipOwner(const bool Val)
{
	bWhiteChipOwner = Val;
}

// === Getters Of Member Objects ===
UHandManager* APlayerPawn::GetHand() const
{
	return HandOfCards;
}
UCashManager* APlayerPawn::GetWallet() const
{
	return Wallet;
}
UTurnStateMachine* APlayerPawn::GetTurnStateMachine() const
{
	return TurnStateMachine;
}

bool APlayerPawn::GetWhiteChipOwner() const
{
	return bWhiteChipOwner;
}
FVector APlayerPawn::GetWhiteChipPosition() const
{
	return WhiteChipPosition->GetComponentLocation();
}

FTransform APlayerPawn::GetCommunityHandPosition() const
{
	return CommunityHandPosition->GetComponentTransform();
}

FTransform APlayerPawn::GetDeckOfCardsPosition() const
{
	return DeckOfCardsPosition->GetComponentTransform();
}

EPlayerActions APlayerPawn::GetTurnAction() const
{
	return TurnStateMachine->GetCurrentAction();
}
EPlayerTurnState APlayerPawn::GetTurnState() const
{
	return TurnStateMachine->GetCurrentState();
}

float APlayerPawn::GetBet() const
{
	return Bet;
}
float APlayerPawn::GetRaiseAmount() const
{
	return RaiseAmount;
}
FString APlayerPawn::GetPlayerName() const
{
	return PlayerName;
}
bool APlayerPawn::GetFolded() const
{
	return bFolded;
}
bool APlayerPawn::GetAllIn() const
{
	return bAllIn;
}

void APlayerPawn::Init()
{
	GameMode = Cast<APokerGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	InitialGameInfo = GameMode->GetInitialInfo();
	
	HandOfCards->SetNumberOfCards(InitialGameInfo.NumOfCardsHeldByPlayers);
	HandOfCards->AddCardPosition(HandOnDeskPosition->GetComponentTransform());
	FVector NewLocation = HandOnDeskPosition->GetRelativeLocation() + FVector(0.0f, 4.8f, 0.0f);
	FRotator NewRotation = HandOnDeskPosition->GetRelativeRotation() + FRotator(0.0f, -4.0f, 0.0f);
	HandOnDeskPosition->SetRelativeLocationAndRotation(NewLocation, NewRotation);
	HandOfCards->AddCardPosition(HandOnDeskPosition->GetComponentTransform());

	Wallet->SetInitialCash(InitialGameInfo.WalletInitialCash);
	Wallet->InitChips(WalletPosition->GetComponentTransform(), PokerChipClass);
}

void APlayerPawn::ResetDefault()
{
	Camera->SetRelativeRotation(FRotator::ZeroRotator);
	
	EmptyHand();
	HandOfCards->HideHand();

	Wallet->SetInitialCash(InitialGameInfo.WalletInitialCash);
	Wallet->GetChipManager()->ReInit(Wallet->GetCash());

	TurnStateMachine->ResetStates();
	ActionCondition->SetCondition(EPlayerActionCondition::PAC_DefaultGame);

	
	bFolded = false;
	bAllIn = false;
	bRaiseActive = false;
	bGaveBigBlind = false;
	
	Bet = 0.0f;
	RaiseAmount = 0.0f;
	RaiseAmountBackup = 0.0f;
}
