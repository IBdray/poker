// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CommunityHand.generated.h"

class UHandManager;
class APlayingCard;

UCLASS()
class POKER_API ACommunityHand : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACommunityHand();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Scene;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UHandManager* Hand;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Spacing;

private:
	int32 MaxNumOfCards;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetHandPosition(const FTransform& Position);

	UFUNCTION(BlueprintCallable)
	void AddCard(APlayingCard* Card);
	UFUNCTION(BlueprintCallable)
	void EmptyHand();

	UFUNCTION(BlueprintCallable)
	UHandManager* GetHand() const;

	UFUNCTION(BlueprintCallable)
	void Init();
};
