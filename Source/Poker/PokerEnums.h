// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PokerEnums.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EChipColor : uint8
{
 CC_Red               UMETA(DisplayName = "Red"),
 CC_Blue              UMETA(DisplayName = "Blue"),
 CC_Green             UMETA(DisplayName = "Green"),
 CC_Black             UMETA(DisplayName = "Black"),
};

UENUM(BlueprintType)
enum class EPlayerTurnState : uint8
{
 PTS_ActiveMove       UMETA(DisplayName = "Active Turn"),
 PTS_MadeMove         UMETA(DisplayName = "Made Move"),
 PTS_NotMadeMove      UMETA(DisplayName = "Not Made Mode"),
};

UENUM(BlueprintType)
enum class EPlayerActions : uint8
{
 PA_Default           UMETA(DisplayName = "Waiting..."),
 PA_Call              UMETA(DisplayName = "Call"),
 PA_Check             UMETA(DisplayName = "Check"),
 PA_AllIn             UMETA(DisplayName = "All In"),
 PA_Raise             UMETA(DisplayName = "Raise"),
 PA_ReRaise           UMETA(DisplayName = "Re-Raise"),
 PA_CheckRaise        UMETA(DisplayName = "Check-Raise"),
 PA_Fold              UMETA(DisplayName = "Fold"),
 PA_SmallBlind        UMETA(DisplayName = "Small Blind"),
 PA_BigBlind          UMETA(DisplayName = "Big Blind"),
};

UENUM(BlueprintType)
enum class EPlayerActionCondition : uint8
{
 PAC_DefaultGame      UMETA(DisplayName = "Default Game"),
 PAC_Menu             UMETA(DisplayName = "Menu"),
 PAC_RaiseBet         UMETA(DisplayName = "Raise Bet"),
};

UENUM(BlueprintType)
enum class ETurnResults : uint8
{
 TR_NextTurn          UMETA(DisplayName = "Next Turn"),
 TR_NextRound         UMETA(DisplayName = "Next Round"),
 TR_EndMatch          UMETA(DisplayName = "End Match"),
 TR_AllInRound        UMETA(DisplayName = "All In Round"),
};

UENUM(BlueprintType)
enum class ERoundCounter : uint8
{
 RC_ZeroRound         UMETA(DisplayName = "Zero Round"),
 RC_FirstRound        UMETA(DisplayName = "First Round"),
 RC_SecondRound       UMETA(DisplayName = "Second Round"),
 RC_ThirdRound        UMETA(DisplayName = "Third Round"),
 RC_MatchEnd          UMETA(DisplayName = "Match End"),
};

UENUM(BlueprintType)
enum class EMatchResult : uint8
{
 MR_Win               UMETA(DisplayName = "Win"),
 MR_Lose              UMETA(DisplayName = "Lose"),
 MR_Tie               UMETA(DisplayName = "Tie"),
};
