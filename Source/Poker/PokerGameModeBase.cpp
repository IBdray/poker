// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "PokerGameModeBase.h"

#include "CashManager.h"
#include "ChipManager.h"
#include "DeckOfCards.h"
#include "PlayerPawn.h"
#include "PlayersManager.h"
#include "Pot.h"
#include "CommunityHand.h"
#include "HandManager.h"
#include "WhiteChip.h"
#include "Kismet/GameplayStatics.h"

APokerGameModeBase::APokerGameModeBase()
{
	RoundCounter = ERoundCounter::RC_ZeroRound;
	bOpeningRaise = true;
	bEnableBlinds = true;
	
	bEnableRaiseCap = true;
	NumOfRaisesBeforeCap = 4;

	// === Delay Properties ===
	StartMathDelay = 2.0f;
	InitHandsDelay = 1.0f;
	SmallBlindDelay = 1.0f;
	BigBlindDelay = 1.0f;
	CommunityCardsDelay = 2.0f;
	NextMatchDelay = 2.0f;
	FinishMatchDelay = 0.5f;
	InitPlayerHandsDelay = 0.5f;
	PlaceCommunityCardsDelay = 0.3f;
	
	// === Chip Values ===
	RedChipValue = 5.0f;
	BlueChipValue = 10.0f;
	GreenChipValue = 25.0f;
	BlackChipValue = 100.0f;

	// === Initial Values ===
	NumOfCardsHeldByPlayers = 2;
	NumOfCommunityCards = 5;
	WalletInitialCash = 500.0f;
	PotInitialCash = 0.0f;
	SmallBlindValue = RedChipValue;
	BigBlindValue = RedChipValue * 2;
	MinRaiseAmount = RedChipValue;
	NumOfChipTypes = 4;

	// === Class Defaults ===
	DeckClass = ADeckOfCards::StaticClass();
	PlayersManagerClass = APlayersManager::StaticClass();
	WhiteChipClass = AWhiteChip::StaticClass();
	CommunityHandClass = ACommunityHand::StaticClass();
	PotClass = APot::StaticClass();

	PotPosition = FTransform(FRotator::ZeroRotator, FVector(-167.9f,-368.2f,95.8f));
}

void APokerGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}

void APokerGameModeBase::StartPokerGame()
{
	PlayersManager = GWorld->SpawnActor<APlayersManager>(PlayersManagerClass, FVector::ZeroVector, FRotator::ZeroRotator);
	DeckOfCards = GWorld->SpawnActor<ADeckOfCards>(DeckClass, PotPosition.GetLocation(), PotPosition.Rotator());
	WhiteChip = GWorld->SpawnActor<AWhiteChip>(WhiteChipClass, PotPosition.GetLocation(), PotPosition.Rotator());
	CommunityHand = GWorld->SpawnActor<ACommunityHand>(CommunityHandClass, FVector::ZeroVector, FRotator::ZeroRotator);
	Pot = GWorld->SpawnActor<APot>(PotClass, PotPosition.GetLocation(), PotPosition.Rotator());

	PlayersManager->Init();
	CommunityHand->Init();
	Pot->Init();
}

void APokerGameModeBase::InitMatch()
{
	bGameStarted = true;
	OnMatchInit();
	const auto HostPlayer = PlayersManager->SetNewHost();

	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [this, HostPlayer]()
	{
		if (bGameStarted)
		{
			WhiteChip->GiveTo(HostPlayer);
			CommunityHand->SetHandPosition(HostPlayer->GetCommunityHandPosition());
			DeckOfCards->Place(HostPlayer->GetDeckOfCardsPosition());
			
			Pot->PrepareForNewMatch();
			PlayersManager->PrepareToNewMatch();
			
			StartMatch();
		}
	}, 0.1f, false, StartMathDelay);
}

void APokerGameModeBase::StartMatch()
{
	RoundCounter = ERoundCounter::RC_ZeroRound;
	OnMatchStart();

	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [&]()
	{
		if (bGameStarted)
		{
			InitPlayersHands();

		if (bEnableBlinds)
		{
			FTimerHandle SmallBlindTimer;
			GetWorldTimerManager().SetTimer(SmallBlindTimer, [this]()
			{
				if (bGameStarted)
				{
					if (const auto SmallBlindPlayer = PlayersManager->NextTurn())
					{
						SmallBlindPlayer->SmallBlind();
						bSmallBlindGiven = true;
						float Delay = SmallBlindPlayer->GetWallet()->GetChipsMovementDelay();

						FTimerHandle BigBlindTimer;
						GetWorldTimerManager().SetTimer(BigBlindTimer, [this]()
						{
							if (bGameStarted)
							{
								// Get Current Player instead of Next Turn because small and big blinds end player's turn
								if (const auto BigBlindPlayer = PlayersManager->CurrentPlayer())
								{
									BigBlindPlayer->BigBlind();
									bBigBlindGiven = true;
									bOpeningRaise = true;
								}
							}
						}, 0.1f, false, Delay + BigBlindDelay);
					}
				}
			}, 0.1f, false, SmallBlindDelay);
		}
		else
			PlayersManager->NextTurn();
		}		
	}, 0.1f, false, InitHandsDelay);
}

void APokerGameModeBase::FirstRound()
{
	RoundCounter = ERoundCounter::RC_FirstRound;
	OnFirstRound();

	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [this]()
	{
		if (bGameStarted)
		{
			PlaceCommunityCards(3);

			if (bAllIn)
				NextRound();
			else
				PlayersManager->NextTurn();
		}
	}, 0.1f, false, CommunityCardsDelay);
}

void APokerGameModeBase::SecondRound()
{
	RoundCounter = ERoundCounter::RC_SecondRound;
	OnSecondRound();
	
	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [this]()
	{
		if (bGameStarted)
		{
			PlaceCommunityCards(1);
		
			if (bAllIn)
				NextRound();
			else
				PlayersManager->NextTurn();
		}
	}, 0.1f, false, CommunityCardsDelay);
}

void APokerGameModeBase::ThirdRound()
{
	RoundCounter = ERoundCounter::RC_ThirdRound;
	OnThirdRound();
	
	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [this]()
	{
		if (bGameStarted)
		{
			PlaceCommunityCards(1);
		
			if (bAllIn)
				NextRound();
			else
				PlayersManager->NextTurn();
		}
	}, 0.1f, false, CommunityCardsDelay);
}

void APokerGameModeBase::AllInRound()
{
	bAllIn = true;
	OnAllInRound();

	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [this]()
	{
		if (bGameStarted)
		{
			PlayersManager->OpenPlayersHands();
			NextRound();
		}
	}, 0.1f, false, AllInRoundDelay);
}

void APokerGameModeBase::FinishMatch()
{
	RoundCounter = ERoundCounter::RC_MatchEnd;
	const TArray<APlayerPawn*> AlivePlayers = PlayersManager->GetAlivePlayers();
	WinnersArray = CalcWinners(AlivePlayers);
	OnMatchFinish();

	if (!PlayersManager->CheckAllPlayersFolded())
	{
		PlayersManager->OpenPlayersHands();
		OnHandOpening();
	}
	
	float WinCash = TakeFromPot();
	TArray<APokerChip*> WinChips = Pot->TakeAllChips();

	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [this, AlivePlayers, WinCash, WinChips]()
	{
		if (bGameStarted) SplitPot(AlivePlayers, WinCash, WinChips);
	}, 0.1f, false, FinishMatchDelay);
}

void APokerGameModeBase::EndPokerGame()
{
	OnGameEnd();

	PlayersManager->ResetPlayersMatchStates();
	bGameStarted = false;
	bAllIn = false;
	bSmallBlindGiven = false;
	bBigBlindGiven = false;

	bOpeningRaise = true;
	RaiseCapCounter = 0;

	LastBet = 0.0f;
	MinRaise = 0.0f;
	
	WinnersArray.Empty();
	
	PlayersManager->ResetDefault();
	WhiteChip->FreeIfOwned();
	CommunityHand->EmptyHand();
	Pot->ResetDefault();
}

void APokerGameModeBase::SplitPot(TArray<APlayerPawn*> Players, float CashPrize, TArray<APokerChip*> ChipsPrize)
{
	if (!ChipsPrize.IsEmpty() && Players.Num() == 1)
	{
		if (ChipsPrize.Num() > 0)
		{
			Players[0]->GetWallet()->GetChipManager()->AddChips(ChipsPrize);
			const float ChipsMovementDelay = Players[0]->GetWallet()->GetChipManager()->MoveChipsSetRandom(ChipsPrize, false);
			
			OnWinChipsStartMovement();
			FTimerHandle Timer;
			GetWorldTimerManager().SetTimer(Timer, [this]()
			{
				OnWinChipsEndMovement();
			}, 0.1f, false, ChipsMovementDelay);
		}
		if (CashPrize > 0)
			Players[0]->AddToWallet(CashPrize);
	}
	else
	{
		if (!ChipsPrize.IsEmpty())
		{
			auto Winners = CalcWinners(Players);
			auto LowestBetPlayer = GetLowestBetPlayer(Winners);
			float MaxPrizeAmount = CalcMaxPrizeAmount(LowestBetPlayer);

			for (auto Winner : Winners)
			{
				float MoneyPrize = FMath::RoundFromZero(FMath::Clamp(CashPrize, 0.0f, MaxPrizeAmount) / Winners.Num());
				float ChipsAmount = CalcChipsPrizeFromCashPrize(CashPrize, MoneyPrize, ChipsPrize.Num());
				auto ChipsSet = SeparateChips(ChipsPrize, ChipsAmount);
			
				Winner->GetWallet()->GetChipManager()->AddChips(ChipsSet);
				const float ChipsMovementDelay = Winner->GetWallet()->GetChipManager()->MoveChipsSetRandom(ChipsSet, false);
				Winner->AddToWallet(MoneyPrize);
				CashPrize -= MoneyPrize;

				OnWinChipsStartMovement();
				FTimerHandle Timer;
				GetWorldTimerManager().SetTimer(Timer, [this]()
				{
					OnWinChipsEndMovement();
				}, 0.1f, false, ChipsMovementDelay);
			}

			Players.Remove(LowestBetPlayer);
			SplitPot(Players, CashPrize, ChipsPrize);
		}
	}
}

TArray<APlayerPawn*> APokerGameModeBase::CalcWinners(const TArray<APlayerPawn*>& Players)
{
	APlayerPawn* Winner = nullptr;
	TArray<APlayerPawn*> Winners;
	
	for (auto Player : Players)
	{
		if (Player->GetFolded()) continue;
		if (!Winner) Winner = Player;
		if (Player == Winner) continue;
		
		const auto MatchupResult = Player->GetHand()->CompareHands(Winner->GetHand());
		switch (MatchupResult)
		{
		case EMatchResult::MR_Win:
			Winner = Player;
			continue;
		case EMatchResult::MR_Tie:
			Winners.Emplace(Player);
		case EMatchResult::MR_Lose: {}
		}
	}

	Winners.Emplace(Winner);
	return Winners;
}

APlayerPawn* APokerGameModeBase::GetLowestBetPlayer(const TArray<APlayerPawn*>& Players)
{
	APlayerPawn* LowestBetPlayer = nullptr;
	for (auto Player : Players)
	{
		if (!LowestBetPlayer)
		{
			LowestBetPlayer = Player;
			continue;
		}

		if (Player->GetBet() < LowestBetPlayer->GetBet())
			LowestBetPlayer = Player;
	}

	return LowestBetPlayer;
}

float APokerGameModeBase::CalcMaxPrizeAmount(const APlayerPawn* Player)
{
	float MaxPrizeAmount = Player->GetBet();
	auto Players = PlayersManager->GetPlayers();
	for (auto Actor : Players)
	{
		APlayerPawn* PlayerTemp = Cast<APlayerPawn>(Actor);
		if (PlayerTemp == Player) continue;

		MaxPrizeAmount += FMath::Clamp(PlayerTemp->GetBet(), 0.0f, Player->GetBet());
	}

	return MaxPrizeAmount;
}

int32 APokerGameModeBase::CalcChipsPrizeFromCashPrize(float AllPrize, float PrizePart, int32 ChipsNum)
{
	float PrizePercent = PrizePart / AllPrize * 100.0f;
	float CashPercent = static_cast<float>(ChipsNum) / 100.0f * PrizePercent;
	return FMath::RoundHalfFromZero(CashPercent);
}

TArray<APokerChip*> APokerGameModeBase::SeparateChips(TArray<APokerChip*>& ChipsSet, int32 AmountToSeparate)
{
	TArray<APokerChip*> SeparatedChips;
	SeparatedChips.Reserve(AmountToSeparate);

	for (int32 i = 0; i < AmountToSeparate; ++i)
	{
		if (!ChipsSet.IsEmpty())
		{
			SeparatedChips.Emplace(ChipsSet.Last());
			ChipsSet.Last() = nullptr;
			ChipsSet.RemoveAt(ChipsSet.Num() - 1);
		}
	}

	return SeparatedChips;
}

void APokerGameModeBase::Turn()
{
	switch (PlayersManager->CheckTurnResults())
	{
		case ETurnResults::TR_NextTurn:
			{
				PlayersManager->NextTurn();
				break;
			}
		case ETurnResults::TR_NextRound:
			{
				NextRound();
				break;
			}
		case ETurnResults::TR_AllInRound:
			{
				if (bAllIn)
					NextRound();
				else
					AllInRound();
				break;
			}
		case ETurnResults::TR_EndMatch:
			{
				FinishMatch();
			}
	}
}

void APokerGameModeBase::NextRound()
{
	PlayersManager->ResetPlayersTurnStates();
	bOpeningRaise = true;
	RaiseCapCounter = 0;

	switch (RoundCounter)
	{
		case ERoundCounter::RC_ZeroRound:
			FirstRound();
			break;
		case ERoundCounter::RC_FirstRound:
			SecondRound();
			break;
		case ERoundCounter::RC_SecondRound:
			ThirdRound();
			break;
		case ERoundCounter::RC_ThirdRound:
		case ERoundCounter::RC_MatchEnd:
			FinishMatch();
	}
}

void APokerGameModeBase::NextMatch()
{
	const float ResetDelay = PlayersManager->ResetPlayersMatchStates();
	bGameStarted = false;
	bAllIn = false;
	bSmallBlindGiven = false;
	bBigBlindGiven = false;

	bOpeningRaise = true;
	RaiseCapCounter = 0;

	LastBet = 0.0f;
	MinRaise = 0.0f;
	
	WinnersArray.Empty();

	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, [this]()
	{
		InitMatch();
	}, 0.1f, false, ResetDelay + NextMatchDelay);
}

bool APokerGameModeBase::CheckCanRaise() const
{
	if (bEnableRaiseCap)
		return RaiseCapCounter < NumOfRaisesBeforeCap;

	return true;
}

void APokerGameModeBase::IncrementRaiseCounter()
{
	if (!bOpeningRaise)
		RaiseCapCounter += 1;
	
	bOpeningRaise = false;
}

// === Pot Operations ===
void APokerGameModeBase::AddToPot(const float Amount)
{
	Pot->AddCash(Amount);
}

float APokerGameModeBase::TakeFromPot()
{
	return Pot->TakeAllCash();
}

void APokerGameModeBase::SetLastBet(const float Amount)
{
	if (Amount > LastBet) LastBet = Amount;
}

void APokerGameModeBase::SetMinRaise(const float Amount)
{
	if (Amount > MinRaise) MinRaise = Amount;
}

APot* APokerGameModeBase::GetPot() const
{
	return Pot;
}

ADeckOfCards* APokerGameModeBase::GetDeckOfCards() const
{
	return DeckOfCards;
}

APlayersManager* APokerGameModeBase::GetPlayersManager() const
{
	return PlayersManager;
}

AWhiteChip* APokerGameModeBase::GetWhiteChip() const
{
	return WhiteChip;
}

ACommunityHand* APokerGameModeBase::GetCommunityHand() const
{
	return CommunityHand;
}

// === Getters ===
bool APokerGameModeBase::GetGameStarted() const
{
	return bGameStarted;
}

bool APokerGameModeBase::GetAllIn() const
{
	return bAllIn;
}

bool APokerGameModeBase::GetSmallBlindGiven() const
{
	return bSmallBlindGiven;
}

bool APokerGameModeBase::GetBigBlindGiven() const
{
	return bBigBlindGiven;
}

const TArray<APlayerPawn*>& APokerGameModeBase::GetWinners() const
{
	return WinnersArray;
}

FPokerChipsValue APokerGameModeBase::GetChipsValue() const
{
	FPokerChipsValue ChipsValue;
	ChipsValue.RedChipValue = RedChipValue;
	ChipsValue.BlueChipValue = BlueChipValue;
	ChipsValue.GreenChipValue = GreenChipValue;
	ChipsValue.BlackChipValue = BlackChipValue;
	
	return ChipsValue;
}
FInitialGameInfo APokerGameModeBase::GetInitialInfo() const
{
	FInitialGameInfo InitialInfo;
	InitialInfo.NumOfCardsHeldByPlayers = NumOfCardsHeldByPlayers;
	InitialInfo.NumOfCommunityCards = NumOfCommunityCards;
	InitialInfo.WalletInitialCash = WalletInitialCash;
	InitialInfo.PotInitialCash = PotInitialCash;
	InitialInfo.SmallBlindValue = SmallBlindValue;
	InitialInfo.BigBlindValue = BigBlindValue;
	InitialInfo.MinRaiseAmount = MinRaiseAmount;
	InitialInfo.NumOfChipTypes = NumOfChipTypes;
	
	return InitialInfo;
}
float APokerGameModeBase::GetLastBet() const
{
	return LastBet;
}

float APokerGameModeBase::GetMinRaise() const
{
	return MinRaise;
}

// === Initializations ===
void APokerGameModeBase::InitPlayersHands()
{
	for (int32 i = 0; i < PlayersManager->GetNumberOfPlayers() * 2; ++i)
	{
		APlayerPawn* Player = PlayersManager->NextPlayer();
	
		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer, [this, Player, i]()
		{
			if (bGameStarted)
			{
				Player->AddCard(DeckOfCards->TakeRandomCard());

				if (i == PlayersManager->GetNumberOfPlayers() * 2 - 1)
					PlayersManager->RankPlayersHands();
			}
		}, 0.1f, false, InitPlayerHandsDelay + i * InitPlayerHandsDelay);
	}
}

void APokerGameModeBase::PlaceCommunityCards(const int32 NumOfCards)
{	
	for (int32 i = 0; i < NumOfCards; ++i)
	{
		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer, [this, NumOfCards, i]()
			{
				if (bGameStarted)
				{
					CommunityHand->AddCard(DeckOfCards->TakeRandomCard());
					OnCardToHandAddition();

					if (i == NumOfCards - 1)
						PlayersManager->RankPlayersHands();
				}
			}, 0.1f, false, PlaceCommunityCardsDelay + i * PlaceCommunityCardsDelay);
	}
}
