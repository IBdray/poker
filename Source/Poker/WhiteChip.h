// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Actor.h"
#include "WhiteChip.generated.h"

class APlayerPawn;


UCLASS()
class POKER_API AWhiteChip : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWhiteChip();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void TimelineMovementProcess(float Value);
	UFUNCTION()
	void TimelineMovementFinish();
	UFUNCTION()
	void MovementEndSound();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Scene;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAudioComponent* Audio;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCurveFloat* MovementCurve;

	TWeakObjectPtr<APlayerPawn> OwnedBy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeOfMovementEndSound;

private:
	FTransform MovementPosition;
	FTimeline MovementTimeline;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void GiveTo(APlayerPawn* Player);

	UFUNCTION(BlueprintImplementableEvent)
	void OnEndMovement();
	
	UFUNCTION(BlueprintCallable)
	bool CheckIfOwned();
	UFUNCTION(BlueprintCallable)
	void FreeIfOwned();

};
