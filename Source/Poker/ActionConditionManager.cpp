// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "ActionConditionManager.h"

// Sets default values for this component's properties
UActionConditionManager::UActionConditionManager()
{
	PrimaryComponentTick.bCanEverTick = false;
	ActionCondition = EPlayerActionCondition::PAC_DefaultGame;
	// ...
}


// Called when the game starts
void UActionConditionManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

EPlayerActionCondition UActionConditionManager::GetCurrentCondition() const
{
	return ActionCondition;
}

void UActionConditionManager::SetCondition(EPlayerActionCondition Condition)
{
	ActionCondition = Condition;
}

bool UActionConditionManager::CheckDefaultCondition()
{
	return (ActionCondition == EPlayerActionCondition::PAC_DefaultGame);
}

bool UActionConditionManager::CheckMenuCondition()
{
	return (ActionCondition == EPlayerActionCondition::PAC_Menu);
}

bool UActionConditionManager::CheckRaiseCondition()
{
	return (ActionCondition == EPlayerActionCondition::PAC_RaiseBet);
}

