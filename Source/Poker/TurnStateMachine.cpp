// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "TurnStateMachine.h"


// Sets default values for this component's properties
UTurnStateMachine::UTurnStateMachine()
{
	PrimaryComponentTick.bCanEverTick = false;
	TurnState = EPlayerTurnState::PTS_NotMadeMove;
	TurnAction = EPlayerActions::PA_Default;
}


// Called when the game starts
void UTurnStateMachine::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

bool UTurnStateMachine::ChangeState(const EPlayerTurnState State)
{
	switch (State)
	{
		case EPlayerTurnState::PTS_ActiveMove:
			{
				if (TurnState == EPlayerTurnState::PTS_NotMadeMove && TurnAction != EPlayerActions::PA_Fold)
					TurnState = EPlayerTurnState::PTS_ActiveMove;
				return true;
			}
		case EPlayerTurnState::PTS_MadeMove:
			{
				if (TurnState == EPlayerTurnState::PTS_ActiveMove) TurnState = EPlayerTurnState::PTS_MadeMove;
				return true;
			}
		case EPlayerTurnState::PTS_NotMadeMove:
			{
				if (TurnState == EPlayerTurnState::PTS_MadeMove && TurnAction != EPlayerActions::PA_Fold)
					TurnState = EPlayerTurnState::PTS_NotMadeMove;
				return true;
			}
	}
	return false;
}

bool UTurnStateMachine::ChangeAction(const EPlayerActions Action)
{
	if (TurnAction != EPlayerActions::PA_Fold && TurnState != EPlayerTurnState::PTS_MadeMove)
	{
		if (Action == EPlayerActions::PA_AllIn)
			TurnAction = EPlayerActions::PA_AllIn;
		else
		{
			if (TurnAction == EPlayerActions::PA_Check && Action == EPlayerActions::PA_Raise)
				TurnAction = EPlayerActions::PA_CheckRaise;
			else if (TurnAction == EPlayerActions::PA_Raise && Action == EPlayerActions::PA_Raise)
				TurnAction = EPlayerActions::PA_ReRaise;
			else
				TurnAction = Action;
		}
		return true;
	}
	return false;
}

void UTurnStateMachine::ResetTurn()
{
	ChangeState(EPlayerTurnState::PTS_NotMadeMove);
	ChangeAction(EPlayerActions::PA_Default);
}
void UTurnStateMachine::ResetStates()
{
	TurnState = EPlayerTurnState::PTS_NotMadeMove;
	TurnAction = EPlayerActions::PA_Default;
}

bool UTurnStateMachine::CheckActiveMove() const
{
	return (TurnState == EPlayerTurnState::PTS_ActiveMove);
}
bool UTurnStateMachine::CheckMadeMove() const
{
	return (TurnState == EPlayerTurnState::PTS_MadeMove);
}
bool UTurnStateMachine::CheckNotMadeMove() const
{
	return (TurnState == EPlayerTurnState::PTS_NotMadeMove);
}
bool UTurnStateMachine::CheckCanMove() const
{
	return (CheckActiveMove() && TurnAction != EPlayerActions::PA_Fold);
}

EPlayerTurnState UTurnStateMachine::GetCurrentState() const
{
	return TurnState;
}
EPlayerActions UTurnStateMachine::GetCurrentAction() const
{
	return TurnAction;
}

