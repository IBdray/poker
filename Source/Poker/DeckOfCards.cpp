// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "DeckOfCards.h"

#include "PlayingCard.h"
#include "Components/AudioComponent.h"
#include "Containers/Array.h"

// Sets default values
ADeckOfCards::ADeckOfCards()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TimeOfMovementEndSound = 0.9f;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Scene);

	Audio = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));
	Audio->SetupAttachment(Scene);
	Audio->bAutoActivate = false;

	// === Class Defaults ===
	CardsClass = APlayingCard::StaticClass();
}

// Called when the game starts or when spawned
void ADeckOfCards::BeginPlay()
{
	Super::BeginPlay();
	
	InitCardsArray();
	FOnTimelineFloat ProgressFunction;
	ProgressFunction.BindUFunction(this, TEXT("TimelineMovementProcess"));
	MovementTimeline.AddInterpFloat(MovementCurve, ProgressFunction);

	FOnTimelineEvent OnTimelineFinishedFunction;
	OnTimelineFinishedFunction.BindUFunction(this, TEXT("TimelineMovementFinish"));
	MovementTimeline.SetTimelineFinishedFunc(OnTimelineFinishedFunction);

	FOnTimelineEvent OnTimelineEnd;
	OnTimelineEnd.BindUFunction(this, TEXT("MovementEndSound"));
	MovementTimeline.AddEvent(TimeOfMovementEndSound, OnTimelineEnd);

	MovementTimeline.SetTimelineLengthMode(TL_LastKeyFrame);
}

void ADeckOfCards::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MovementTimeline.IsPlaying())
		MovementTimeline.TickTimeline(DeltaTime);
}

void ADeckOfCards::TimelineMovementProcess(float Value)
{
	FVector Location = FMath::Lerp(GetActorLocation(), MovementPosition.GetLocation(), Value);
	FRotator Rotation = FMath::Lerp<FRotator>(GetActorRotation(), MovementPosition.Rotator(), Value);
	SetActorLocationAndRotation(Location, Rotation);
}

void ADeckOfCards::TimelineMovementFinish()
{
	UpdateCardsPosition();
}

void ADeckOfCards::MovementEndSound()
{
	OnEndMovement();
}

void ADeckOfCards::InitCardsArray()
{
	CardsArray.Reserve(52);
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 13; j++)
		{
			FString CardPower = UEnum::GetValueAsString(static_cast<ECardPower>(j));
			CardPower.RemoveAt(0,15);	// Make form ECardPower::CP_Ace - Ace
			FString CardSuit = UEnum::GetValueAsString(static_cast<ECardSuit>(i));
			CardSuit.RemoveAt(0,14);		// Make form ECardSuit::CS_Diamonds - Diamonds

			APlayingCard* Card = GWorld->SpawnActor<APlayingCard>(CardsClass, FVector::ZeroVector, FRotator::ZeroRotator);
			Card->Init(static_cast<ECardSuit>(i), static_cast<ECardPower>(j), CardPower + "Of" + CardSuit);
			Card->SetActorTransform(GetActorTransform());
			Card->SetDeckOfCards(this);
			
			CardsArray.Emplace(Card);
		}
}

void ADeckOfCards::ReturnCard(APlayingCard* Card)
{
	CardsArray.Emplace(Card);
	Card->RiseAndMove(GetActorTransform());
}

void ADeckOfCards::UpdateCardsPosition()
{
	for (APlayingCard* Card : CardsArray)
		Card->SetActorLocationAndRotation(GetActorLocation(), GetActorRotation());
}

void ADeckOfCards::Place(const FTransform& Position)
{
	MovementPosition = Position;
	MovementTimeline.PlayFromStart();
	OnStartMovement();
}

APlayingCard* ADeckOfCards::GetLastCard() const
{
	return CardsArray.Last();
}

APlayingCard* ADeckOfCards::GetRandomCard() const
{
	return CardsArray[FMath::RandRange(0, CardsArray.Num() - 1)];
}

APlayingCard* ADeckOfCards::TakeRandomCard()
{
	const int32 RandomNumber = FMath::RandRange(0, CardsArray.Num() - 1);
	APlayingCard* Temp = CardsArray[RandomNumber];
	CardsArray[RandomNumber] = nullptr;
	CardsArray.RemoveAt(RandomNumber);
	return Temp;
}

