// Copyright (c) 2022 Ivan Babanov (IB Dray). No Rights Reserved.


#include "PlayersManager.h"

#include "CashManager.h"
#include "ChipManager.h"
#include "HandManager.h"
#include "CommunityHand.h"
#include "TurnStateMachine.h"
#include "PlayerPawn.h"
#include "PokerGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayersManager::APlayersManager()
{
	PrimaryActorTick.bCanEverTick = false;
	ResetPlayerDelay = 0.5f;

}

// Called when the game starts or when spawned
void APlayersManager::BeginPlay()
{
	Super::BeginPlay();
}

APlayerPawn* APlayersManager::SetNewHost()
{
	if (Host)
		HostNum = (++HostNum >= Players.Num()) ? 0 : HostNum;
	else
		HostNum = FMath::RandRange(0, Players.Num() - 1);

	Host = static_cast<APlayerPawn*>(Players[HostNum]);
	ActivePlayerNum = HostNum;
	ActivePlayer = Host;
	return Host;
}

void APlayersManager::PrepareToNewMatch()
{
	for (const auto Player : Players)
		static_cast<APlayerPawn*>(Player)->PrepareToNewMatch();

	bAllPlayersFolded = false;
}

APlayerPawn* APlayersManager::NextTurn()
{	
	APlayerPawn* Player = NextPlayer();
	Player->TakeTurn();
	return Player;
}

ETurnResults APlayersManager::CheckTurnResults()
{
	int32 FoldCounter = 0;
	int32 AllInCounter = 0;
	int32 PartialAllInCounter = 0;
	for (const auto Player : Players)
	{
		const auto PlayerTemp = static_cast<APlayerPawn*>(Player);
		if (PlayerTemp->GetFolded())
		{
			FoldCounter += 1;
			continue;
		}
		if (PlayerTemp->GetAllIn())
		{
			AllInCounter += 1;
			continue;
		}
		if (AllInCounter > 0 && PlayerTemp->CheckEnoughBet())
		{
			PartialAllInCounter +=1;
			continue;
		}
		if (!PlayerTemp->CheckEnoughBet() && !PlayerTemp->GetAllIn() || !PlayerTemp->GetTurnStateMachine()->CheckMadeMove())
			return ETurnResults::TR_NextTurn;
	}
	if (FoldCounter >= Players.Num()-1)
	{
		bAllPlayersFolded = true;
		return ETurnResults::TR_EndMatch;
	}
	if (AllInCounter + PartialAllInCounter >= Players.Num() - FoldCounter)
		return ETurnResults::TR_AllInRound;
	
	return ETurnResults::TR_NextRound;
}

void APlayersManager::ResetPlayersTurnStates()
{
	for (const auto Player : Players)
		static_cast<APlayerPawn*>(Player)->GetTurnStateMachine()->ResetTurn();
}
float APlayersManager::ResetPlayersMatchStates()
{
	for (int32 i = 0; i < Players.Num(); ++i)
	{
		APlayerPawn* Player = static_cast<APlayerPawn*>(Players[i]);

		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer, [=]()
		{
			if (!Player->GetFolded())
				Player->EmptyHand();
			if (Host == Player)
				GameMode->GetCommunityHand()->EmptyHand();
			
			Player->GetTurnStateMachine()->ResetStates();			
		}, 0.1f, false, i * ResetPlayerDelay);
	}
	return (Players.Num() - 1) * ResetPlayerDelay;
}

void APlayersManager::RankPlayersHands()
{
	const auto CommunityHand = GameMode->GetCommunityHand()->GetHand();
	for (const auto Player : Players)
		static_cast<APlayerPawn*>(Player)->GetHand()->HandRanking(CommunityHand->GetCardsArray());
}

void APlayersManager::OpenPlayersHands()
{
	for (int32 i = 0; i < Players.Num(); ++i)
	{
		const APlayerPawn* Player = Cast<APlayerPawn>(Players[i]);
		if (Player->GetFolded()) continue;
		
		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer, [=]()
		{
			Player->GetHand()->OpenHand();
		}, 0.1f, false, i * 0.5f);
	}
}

APlayerPawn* APlayersManager::NextPlayer()
{
	ActivePlayer = static_cast<APlayerPawn*>(Players[NextPlayerNum()]);
	return ActivePlayer;
}
APlayerPawn* APlayersManager::CurrentPlayer() const
{
	return ActivePlayer;
}
APlayerPawn* APlayersManager::PreviousPlayer()
{
	ActivePlayer = static_cast<APlayerPawn*>(Players[PreviousPlayerNum()]);
	return ActivePlayer;
}

int32 APlayersManager::NextPlayerNum()
{
	ActivePlayerNum = (++ActivePlayerNum >= Players.Num()) ? 0 : ActivePlayerNum;
	return ActivePlayerNum;
}
int32 APlayersManager::CurrentPlayerNum() const
{
	return ActivePlayerNum;
}
int32 APlayersManager::PreviousPlayerNum()
{
	ActivePlayerNum = (--ActivePlayerNum < 0) ? Players.Num()-1 : ActivePlayerNum;
	return ActivePlayerNum;
}

int32 APlayersManager::GetNumberOfPlayers() const
{
	return Players.Num();
}

TArray<AActor*> APlayersManager::GetPlayers() const
{
	return Players;
}

TArray<APlayerPawn*> APlayersManager::GetAlivePlayers() const
{
	TArray<APlayerPawn*> AlivePlayers;
	for (auto Player : Players)
	{
		APlayerPawn* PlayerTemp = Cast<APlayerPawn>(Player);
		if (!PlayerTemp->GetFolded())
			AlivePlayers.Emplace(PlayerTemp);
	}

	return AlivePlayers;
}

bool APlayersManager::CheckAllPlayersFolded() const
{
	return bAllPlayersFolded;
}

float APlayersManager::GetMinCash() const
{
	float MinCash = 0;
	for (auto Player : Players)
	{
		APlayerPawn* PlayerTemp = Cast<APlayerPawn>(Player);
		if (PlayerTemp->GetWallet()->GetCash() > MinCash)
			MinCash = PlayerTemp->GetWallet()->GetCash();
	}

	return MinCash;
}

void APlayersManager::Init()
{
	GameMode = Cast<APokerGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerPawn::StaticClass(), Players);
	for (auto Player : Players)
		static_cast<APlayerPawn*>(Player)->Init();
}

void APlayersManager::ResetDefault()
{
	ActivePlayerNum = 0;
	ActivePlayer = nullptr;
	HostNum = 0;
	Host = nullptr;

	bAllPlayersFolded = false;

	for (auto Player : Players)
		static_cast<APlayerPawn*>(Player)->ResetDefault();
}

